# Include local overrides to options.
# You can use this file to configure your dev env. It is ignored by git.
-include options.local.mk  # Prefix with hyphen to tolerate absence of file.

.DEFAULT_GOAL := oneshot
HELP_SPACING ?= 30
PACKAGE_NAME := listaflow
SOURCE_TAG ?= $(TAG)

# Parameters ##################################################################
#
# For ci commmands use the rest as arguments and turn them into do-nothing targets
ifeq ($(firstword $(MAKECMDGOALS)),$(filter $(firstword $(MAKECMDGOALS)),ci-test-image-exists ci-pull-image ci-build-image ci-retag-django-image ci-push-images))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


SHELL_PREFIX=docker-compose exec -e DJANGO_SETTINGS_MODULE=conf.settings.local django
SHELL_NO_TTY_PREFIX=docker-compose exec -T -e DJANGO_SETTINGS_MODULE=conf.settings.local django
SHELL_NO_TTY_PREFIX_SUPER_PASSWORD=docker-compose exec -T -e DJANGO_SUPERUSER_PASSWORD=admin -e DJANGO_SETTINGS_MODULE=conf.settings.local django
BACKEND_HOME=/app/backend

help: ## Display this help message.
	@echo "Please use \`make <target>' where <target> is one of"
	@perl -nle'print $& if m{^[\.a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-$(HELP_SPACING)s\033[0m %s\n", $$1, $$2}'

build: ## Build the docker images
	docker-compose build --no-cache

# Celery beat needs to run after migration.
up: up.without_celery migrate up.celery ## start development server in daemon mode

restart: ## restart development server
	docker-compose restart django celeryworker celerybeat

up.without_celery: ## start development server without celery worker
	docker-compose up -d

up.celery: ## start celery
	docker-compose --profile celery up -d

run: run.without_celery migrate run.celery ## start development server in foreground

run.without_celery: ## start development server without_celery in foreground
	docker-compose up

run.celery: ## start celery in foreground
	docker-compose --profile celery up

logs: ## tail logs
	docker-compose logs -f --tail=300

run.frontend: ## start frontend development server
	npm run start --prefix frontend

stop: ## stop all services
	docker-compose stop

createapplication: ## createapplication management command to create a new application
	${SHELL_PREFIX} ./manage.py createoauthapplication confidential password --skip-authorization --name main-app

seed_data: ## Populate with sample data
	${SHELL_NO_TTY_PREFIX} ./manage.py seed_data

provision: migrate collectstatic compilemessages.backend ## provision database

migrate: ## run migration
	${SHELL_NO_TTY_PREFIX} ./manage.py migrate

collectstatic: ## run collectstatic
	${SHELL_PREFIX} ./manage.py collectstatic -v0 --noinput

makemessages.backend: ## run makemessages for backend
	${SHELL_PREFIX} ./manage.py makemessages --all

compilemessages.backend: ## run compilemessages for backend
	${SHELL_PREFIX} ./manage.py compilemessages

makemessages.frontend: ## run makemessages for frontend
	npm run sync-i18n --prefix frontend

shell: ## Open a shell on the backend container
	${SHELL_PREFIX} /bin/bash

destroy: ## stop and delete all containers
	docker-compose down

oneshot: up createapplication seed_data install_frontend_dependencies build.frontend collectstatic compilemessages.backend ## One shot command to get a local server up and running
	echo "Visit Listaflow at http://localhost:8000"

upgrade: ## upgrade python dependencies
	${SHELL_PREFIX} pip install pip-tools
	${SHELL_PREFIX} python -m piptools compile requirements/requirements.in requirements/constraints.in --output-file=requirements.txt

format: format.backend format.frontend ## format frontend and backend code

format.backend: ## format backend code
	${SHELL_PREFIX} python -m black .

format.frontend: ## format frontend code
	npm run format --prefix frontend

quality.backend: ## run quality checks for backend code
	${SHELL_NO_TTY_PREFIX} python -m black --check .
	${SHELL_NO_TTY_PREFIX} python -m pylint --load-plugins=pylint_django -vj 1 ../backend

quality: quality.backend ## run quality checks for backend code

test.backend: ## run quality checks for backend code
	${SHELL_NO_TTY_PREFIX} python -m pytest -n auto --cov --cov-fail-under=95 .

test.frontend: ## run tests for frontend code
	CI=true npm test --prefix frontend --coverage

test: test.backend test.frontend ## run tests for backend and frontend code

qa: quality test ## run all quality checks and tests

install_frontend_dependencies: ## install frontend dependencies
	npm install --prefix frontend

build.frontend: ## build frontend
	npm run build --prefix frontend

# CI/CD #######################################################################
ci-test-image-exists:
	@export REPO_ID=`curl -s ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/ | jq '.[] | select(.location==env.CI_REGISTRY_IMAGE) | .id'`; \
	test -z $$REPO_ID && exit 0; \
	test `curl -sf -w "%{http_code}" -o /dev/null ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/$$REPO_ID/tags/${RUN_ARGS}-${VERSION}` = 404 || \
	(echo "Image version ${VERSION} already exists. Please bump up the version by incrementing the 'VERSION' variable in '.gitlab-ci.yml'." && exit 1)

ci-pull-image:
	docker pull "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" || docker pull "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-latest" || true

ci-build-image:
	docker build \
		--cache-from "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" \
		--cache-from "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-latest" \
		-t "${CI_REGISTRY_IMAGE}:$(RUN_ARGS)-$(TAG)" \
		-f docker/app/Dockerfile .

ci-retag-django-image:
	@for service in $(RUN_ARGS) ; do \
		docker tag "${CI_REGISTRY_IMAGE}:django-$(SOURCE_TAG)" "${CI_REGISTRY_IMAGE}:$$service-$(TAG)" ; \
	done

ci-push-images:
	@for service in $(RUN_ARGS) ; do \
		docker push "${CI_REGISTRY_IMAGE}:$$service-$(TAG)" ; \
	done
