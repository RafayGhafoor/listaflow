var editors = {};
let schemas = {};

function addSchemaEditor(interfaceSelectorId) {
    let schemaSelector = document.getElementById(interfaceSelectorId);
    let custArgsId = interfaceSelectorId.replace('interface_type', 'customization_args');
    let custArgsElement = document.getElementById(custArgsId);
    let context = JSON.parse(custArgsElement.getAttribute("context"))
    schemas = context.schemas;
    let defaultValues = context.default_values;
    let editorId = "editor-" + custArgsId;

    schemaSelector.addEventListener("change", (event) => {
        let editorValue = defaultValues[event.target.value];
        loadSchemaEditor(editorId, custArgsId, event.target.value, editorValue);
    });

    let initialValue = JSON.parse(context.startval);
    if (!initialValue) {
        initialValue = defaultValues[schemaSelector.value];
    }

    loadSchemaEditor(editorId, custArgsId, schemaSelector.value, initialValue);
}

function loadSchemaEditor(editorId, formId, interface_type, startval){
    if (editors.hasOwnProperty(editorId)) {
        editors[editorId].destroy();
    }

    if (!interface_type) {
        document.getElementById(formId).innerHTML = null;
        return;
    }

    let options = {
        schema: schemas[interface_type],
        disable_edit_json: true,
        disable_collapse: true,
        startval: startval,
        theme: 'bootstrap4',
    }

    let editor = new JSONEditor(
        document.getElementById(editorId), options);

    editors[editorId] = editor;
    editor.on('change', function(e){
        document.getElementById(formId).innerHTML = JSON.stringify(editor.getValue());
    });
};
