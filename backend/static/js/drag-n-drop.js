function tabularInlineDragAndDrop(inlineElement) {
  Sortable.create(inlineElement, {
    filter: ".add-row",
    draggable: `.has_original`,
    onEnd: (event) => {
      updateOrder();
    }
  });
}

function updateOrder() {
  let inlineOrderElements = document.querySelectorAll('.drag-n-drop-enabled');
  let newOrder = 0;
  inlineOrderElements.forEach(inlineOrderElement => {
    inlineOrderElement.value = newOrder;
    newOrder++;
  });
}
