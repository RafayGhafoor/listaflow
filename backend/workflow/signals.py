"""
Signals for Workflow app
"""
import django
from django.db import transaction
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from workflow.models import (
    Checklist,
    ChecklistTask,
    ChecklistTaskDefinition,
    Run,
    RunTrendTaskReport,
)
from workflow.report.json import (
    ChecklistDefinitionTrendsJsonReport,
    RunOverviewJsonReport,
    TemplateRunsJsonReport,
)
from workflow.tasks import (
    compute_task_definition_trends_report,
    flush_cache,
    flush_many_cache,
)


@receiver(post_save, sender=ChecklistTaskDefinition)
def post_save_task_definition(
    sender, instance: ChecklistTaskDefinition, created, **kwargs
):  # pylint: disable=W0613
    """
    Create task trend report every time a ChecklistTaskDefinition is created
    """

    checklist_definition = instance.checklist_definition
    cache_key = ChecklistDefinitionTrendsJsonReport.make_cache_key(checklist_definition)
    flush_many_cache.apply_async(
        args=[
            f"{cache_key}*",
        ]
    )
    runs = checklist_definition.related_runs().all()
    for run in runs:
        if created:
            RunTrendTaskReport.objects.create(task_definition=instance, run=run)


@receiver(post_save, sender=ChecklistTask)
def post_save_task(sender, instance: ChecklistTask, **kwargs):  # pylint: disable=W0613
    """
    Re-compute task trend report after saving task
    """
    compute_task_definition_trends_report.apply_async(args=[instance.definition.id])
    cache_key = TemplateRunsJsonReport.make_cache_key({"id": instance.id})
    flush_cache.apply_async(args=[cache_key])


@receiver(pre_save, sender=Checklist)
def pre_save_checklist(
    sender, instance: Checklist, **kwargs
):  # pylint: disable=unused-argument
    """
    Ensure, upon first save, a checklist will have its run information set.

    Note that this may not be triggered if the checklist is added via the
    'checklists' attribute on a Run. In that case you'll need to either
    set these manually, or just run .save() on the run to trigger its
    save handler, which also handles this.
    """
    if instance.id:
        # This has run before. Skip this time.
        return
    if instance.run:
        instance.start_date = instance.run.start_date
        instance.end_date = instance.run.end_date
        instance.due_date = instance.run.due_date


@receiver(post_save, sender=Checklist)
def post_save_checklist(
    sender, instance: Checklist, created, **kwargs
):  # pylint: disable=W0613
    """
    Every time checklist is changed, flush run report cache
    """
    if not created:
        cache_key = ChecklistDefinitionTrendsJsonReport.make_cache_key(
            instance.definition
        )
        flush_many_cache.apply_async(args=[f"{cache_key}*"])
        if not instance.run:
            return
        overview_cache_key = RunOverviewJsonReport.make_cache_key(instance.run)
        flush_many_cache.apply_async(args=[f"{overview_cache_key}*"])


def recalculate_run_trends(sender, instance: Run, **kwargs):  # pylint: disable=W0613
    """
    When a run is created, trends information of a task definition
    instance is also changed because of checklists creation. Trends report
    recomputation is necessary in this scenario.
    """
    definition = instance.checklist_definition
    with transaction.atomic():
        for task_definition in definition.task_definitions.all():
            task_queryset = (
                instance.related_tasks()
                .filter(definition=task_definition)
                .values("checklist__assignee", "completed")
            )
            RunTrendTaskReport.objects.create(
                task_definition=task_definition,
                run=instance,
                user_completion_map={
                    item["checklist__assignee"]: item["completed"]
                    for item in task_queryset
                },
            )


post_recurrence_run_initialized = django.dispatch.Signal()


post_recurrence_run_initialized.connect(recalculate_run_trends, sender=Run)
