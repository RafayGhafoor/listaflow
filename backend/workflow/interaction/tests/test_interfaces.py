"""
Tests for interfaces module.
"""
from django.core.exceptions import ValidationError
from django.db.models import F

import pytest
from rest_framework.exceptions import ValidationError as RESTValidationError

from workflow.interaction.interfaces import (
    CheckboxInterface,
    LinearScaleRatingInterface,
    MarkdownTextAreaInterface,
    MultiChoiceInterface,
    NumericInterface,
)
from workflow.models import ChecklistTask
from workflow.tests.factories import ChecklistTaskFactory

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_validate_correct_markdown_textarea_customization_args_raises_no_error():
    valid_customization_args = {"placeholder": "Type your answer..."}

    MarkdownTextAreaInterface.validate_customization_args(valid_customization_args)


def test_validate_incorrect_markdown_textarea_customization_args_raises_error():
    invalid_customization_args = {"invalid_key": "Type your answer..."}
    with pytest.raises(ValidationError) as error_info:
        MarkdownTextAreaInterface.validate_customization_args(
            invalid_customization_args
        )

    assert len(error_info.value.messages) == 1
    assert "'placeholder' is a required property" in error_info.value.messages[0]


def test_validate_correct_markdown_textarea_response_raises_no_error():
    customization_args = {"placeholder": "Type your answer..."}

    valid_response = {"content": "Valid response"}

    MarkdownTextAreaInterface.validate_response(valid_response, customization_args)


def test_validate_incorrect_markdown_textarea_response_raises_error():
    customization_args = {"placeholder": "Type your answer..."}

    invalid_response = {"content": 1234}

    with pytest.raises(RESTValidationError) as error_info:
        MarkdownTextAreaInterface.validate_response(
            invalid_response, customization_args
        )

    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    assert "1234 Is Not Of Type 'String'" in error_detail.title()


def test_display_markdown_textarea_response():
    customization_args = {"placeholder": "Type your answer..."}

    empty_response_display = MarkdownTextAreaInterface.to_str({}, customization_args)
    assert empty_response_display == ""

    content = "dummy_content"
    non_empty_response = {"content": content}
    non_empty_response_display = MarkdownTextAreaInterface.to_str(
        non_empty_response, customization_args
    )
    assert non_empty_response_display == content


def test_validate_correct_linear_scale_rate_customization_args_raises_no_error():
    valid_customization_args = {
        "min_value": 1,
        "max_value": 5,
        "max_value_description": "Good",
        "min_value_description": "Bad",
    }

    LinearScaleRatingInterface.validate_customization_args(valid_customization_args)


def test_validate_incorrect_linear_scale_rate_customization_args_raises_error():
    invalid_customization_args = {
        "min_value": -1,  # Expected value <= 0
        "max_value": 5,
        "max_value_description": "Good",
        "min_value_description": "Bad",
    }
    with pytest.raises(ValidationError) as error_info:
        LinearScaleRatingInterface.validate_customization_args(
            invalid_customization_args
        )

    assert len(error_info.value.messages) == 1
    assert "-1 is less than the minimum of 0" in error_info.value.messages[0]


def test_validate_correct_linear_scale_rate_response_raises_no_error():
    customization_args = {
        "min_value": 1,
        "max_value": 5,
        "max_value_description": "Good",
        "min_value_description": "Bad",
    }

    valid_response = {"rating": 4}

    LinearScaleRatingInterface.validate_response(valid_response, customization_args)


def test_validate_incorrect_linear_scale_rate_response_raises_error():
    customization_args = {
        "min_value": 1,
        "max_value": 5,
        "max_value_description": "Good",
        "min_value_description": "Bad",
    }

    invalid_response = {"rating": 6}

    with pytest.raises(RESTValidationError) as error_info:
        LinearScaleRatingInterface.validate_response(
            invalid_response, customization_args
        )

    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    expected_error_message = "Expected User Rating To Be In Between 1 And 5, Found 6"
    assert expected_error_message in error_detail.title()


def test_display_linear_scale_rate_response():
    customization_args = {
        "min_value": 1,
        "max_value": 5,
        "max_value_description": "Good",
        "min_value_description": "Bad",
    }

    empty_response_display = LinearScaleRatingInterface.to_str({}, customization_args)
    assert empty_response_display == ""

    rating = 4
    non_empty_response = {"rating": rating}
    non_empty_response_display = LinearScaleRatingInterface.to_str(
        non_empty_response, customization_args
    )
    assert non_empty_response_display == f"{rating} / {customization_args['max_value']}"


def test_validate_correct_checkbox_customization_args_raises_no_error():
    valid_customization_args = {}

    CheckboxInterface.validate_customization_args(valid_customization_args)


def test_validate_incorrect_checkbox_customization_args_raises_error():
    valid_customization_args = {"extra_key": "random value"}

    with pytest.raises(ValidationError) as error_info:
        CheckboxInterface.validate_customization_args(valid_customization_args)

    assert len(error_info.value.messages) == 1
    error_message_content = (
        "Additional properties are not allowed ('extra_key' was unexpected)"
    )
    assert error_message_content in error_info.value.messages[0]


def test_validate_correct_checkbox_response_raises_no_error():
    customization_args = {}

    valid_response = {"checked": True}

    CheckboxInterface.validate_response(valid_response, customization_args)


def test_validate_incorrect_checkbox_response_raises_error():
    customization_args = {}

    invalid_response = {"checked": 123}

    with pytest.raises(RESTValidationError) as error_info:
        CheckboxInterface.validate_response(invalid_response, customization_args)

    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    expected_error_message = "123 Is Not Of Type 'Boolean'"
    assert expected_error_message in error_detail.title()


def test_display_checkbox_response():
    customization_args = {}

    empty_response_display = CheckboxInterface.to_str({}, customization_args)
    assert empty_response_display == "not checked"

    checked_response = {"checked": True}
    non_empty_response_display = CheckboxInterface.to_str(
        checked_response, customization_args
    )
    assert non_empty_response_display == "checked"


def test_validate_correct_numeric_response_raises_no_error():
    customization_args = {
        "min_value": -1,
        "max_value": 5,
        "step": 1,
    }

    valid_response = {"number": 4}

    NumericInterface.validate_response(valid_response, customization_args)


def test_validate_incorrect_numeric_response_raises_error():
    customization_args = {
        "min_value": 1,
        "max_value": 5,
        "step": 1,
    }

    invalid_response = {"number": 6}

    with pytest.raises(RESTValidationError) as error_info:
        NumericInterface.validate_response(invalid_response, customization_args)

    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    expected_error_message = "Expected value to be less than 5"
    assert expected_error_message in error_detail


def test_validate_no_args_numeric_response_raises_no_error():
    customization_args = {
        "min_value": None,
        "max_value": None,
        "step": None,
    }

    valid_response = {"number": -4}

    NumericInterface.validate_response(valid_response, customization_args)


def test_display_numeric_response():
    customization_args = {
        "min_value": 0.0,
        "max_value": 5.0,
        "step": 0.5,
    }

    empty_response_display = NumericInterface.to_str({}, customization_args)
    assert empty_response_display == ""

    number = 4.0
    non_empty_response = {"number": number}
    non_empty_response_display = NumericInterface.to_str(
        non_empty_response, customization_args
    )
    assert non_empty_response_display == str(number)


def test_validate_incorrect_numeric_customization_args_raises_error():
    customization_args = {
        "min_value": 5.0,
        "max_value": 1.0,
        "step": 0.5,
    }

    with pytest.raises(ValidationError) as error_info:
        NumericInterface.validate_customization_args(customization_args)

    assert len(error_info.value.messages) == 1
    error_message_content = "Maximum value is greater than or equal to minimum"
    assert error_message_content in error_info.value.messages[0]


def test_multi_choice_customization_args_validation():
    customization_args = {
        "minimum_selected": 2,
        "maximum_selected": 3,
        "available_choices": ["a", "b", "c"],
    }
    MultiChoiceInterface.validate_customization_args(customization_args)
    # incorrect maximum_selected
    customization_args["maximum_selected"] = 1
    with pytest.raises(ValidationError):
        MultiChoiceInterface.validate_customization_args(customization_args)
    # incorrect empty string choices
    customization_args["available_choices"] = ["", ""]
    with pytest.raises(ValidationError):
        MultiChoiceInterface.validate_customization_args(customization_args)
    # less number of choices from minimum_selected
    customization_args["maximum_selected"] = 3
    customization_args["available_choices"] = ["a"]
    with pytest.raises(ValidationError):
        MultiChoiceInterface.validate_customization_args(customization_args)


def test_validate_correct_multi_choice_response_raises_no_error():
    customization_args = {
        "minimum_selected": 1,
        "maximum_selected": 2,
        "available_choices": ["a", "b", "c"],
    }
    valid_response = {"selected_choices": ["a", "b"]}
    MultiChoiceInterface.validate_response(valid_response, customization_args)


def test_validate_incorrect_count_multi_choice_response_raises_error():
    customization_args = {
        "minimum_selected": 1,
        "maximum_selected": 2,
        "available_choices": ["a", "b", "c"],
    }
    invalid_response = {"selected_choices": ["a", "b", "c"]}
    with pytest.raises(RESTValidationError) as error_info:
        MultiChoiceInterface.validate_response(invalid_response, customization_args)
    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    assert str(customization_args["maximum_selected"]) in error_detail


def test_validate_incorrect_choice_multi_choice_response_raises_error():
    customization_args = {
        "minimum_selected": 1,
        "maximum_selected": 2,
        "available_choices": ["a", "b", "c"],
    }
    invalid_response = {"selected_choices": ["x"]}
    with pytest.raises(RESTValidationError) as error_info:
        MultiChoiceInterface.validate_response(invalid_response, customization_args)
    assert "response" in error_info.value.detail
    error_detail = error_info.value.detail["response"][0]
    assert "Invalid" in error_detail


def test_multi_choice_completion_conditions():
    customization_args = {
        "minimum_selected": 3,
        "maximum_selected": 4,
        "available_choices": ["a", "b", "c", "d", "e", "f"],
    }
    # less than minimum_selected, should return False
    response = {"selected_choices": ["a"]}
    assert not MultiChoiceInterface.can_mark_completed(response, customization_args)
    # more than maximum_selected, should return False
    response = {"selected_choices": ["a", "b", "c", "d", "e", "f"]}
    assert not MultiChoiceInterface.can_mark_completed(response, customization_args)
    # selected atleast minimum_selected
    response = {"selected_choices": ["a", "c", "e"]}
    assert MultiChoiceInterface.can_mark_completed(response, customization_args)


def test_accummulate_linear_scale_rate_responses():
    """
    Test accummulate function for Linear Scale Rating response
    """

    responses = [{"rating": 4}, {"rating": 2}, {"rating": 3}, {"rating": 2}]
    for response in responses:
        task = ChecklistTaskFactory.create(response=response)
        task.save()

    queryset = ChecklistTask.objects.annotate(user=F("checklist__assignee")).values(
        "response", "user", "completed"
    )

    acc_result = LinearScaleRatingInterface.accumulate_interface_instances(queryset)
    required_keys = ["buckets"]
    for key in required_keys:
        assert key in acc_result

    buckets = acc_result["buckets"]
    assert len(buckets) == 3

    for bucket in buckets:
        value = bucket["rating"]
        users = bucket["users"]
        if value == 4:
            assert len(users) == 1
            continue
        if value == 3:
            assert len(users) == 1
            continue
        if value == 2:
            assert len(users) == 2
            continue
        if value == 1:
            assert len(users) == 0
            continue
        raise AssertionError("invalid bucket")


def test_accummulate_numeric_value_responses():
    """
    Test accummulate function for Numeric
    """
    responses = [{"number": 4.0}, {"number": 2.1}, {"number": 3.2}, {"number": 2.1}]
    for response in responses:
        task = ChecklistTaskFactory.create(response=response)
        task.save()

    queryset = ChecklistTask.objects.annotate(user=F("checklist__assignee")).values(
        "response", "user", "completed"
    )

    acc_result = NumericInterface.accumulate_interface_instances(queryset)
    required_keys = ["buckets"]
    for key in required_keys:
        assert key in acc_result

    buckets = acc_result["buckets"]
    assert len(buckets) == 3

    for bucket in buckets:
        value = bucket["number"]
        users = bucket["users"]
        if value == 4.0:
            assert len(users) == 1
            continue
        if value == 3.2:
            assert len(users) == 1
            continue
        if value == 2.1:
            assert len(users) == 2
            continue
        raise AssertionError("invalid bucket")
