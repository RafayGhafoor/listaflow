"""
Protocols for typing
"""
# pylint: disable=too-few-public-methods
from typing import Protocol

from django.contrib.auth.base_user import AbstractBaseUser
from profiles.models import Team


class AuthoredModel(Protocol):
    """Protocol for models with an author field."""

    author: AbstractBaseUser


class AssignedModel(Protocol):
    """Protocol for models with an assignee field."""

    assignee: AbstractBaseUser


class TeamModel(Protocol):
    """Protocol for model with a team field."""

    team: Team
