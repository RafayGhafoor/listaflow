from django.core.management.base import BaseCommand

import factory.random

from profiles.tests.factories import (
    UserWithNameFactory,
    AdminFactory,
    TeamWithTaggedUsersFactory,
)
from workflow.tests.factories import (
    ChecklistDefinitionWithAllTaskTypes,
    RecurringChecklist,
)

DEFAULT_USER_COUNT = 3
DEFAULT_TEAM_COUNT = 2


class Command(BaseCommand):
    help = "Populate Listaflow with sample data, Users, Teams, Checklists"

    def _set_seed(self, seed):
        self.stdout.write(f"Using seed '{seed}' for randomization.")
        factory.random.reseed_random(seed)

    def handle(self, *args, **options):
        seed = options.get("seed")
        self._set_seed(seed)

        admin = AdminFactory.create()
        self.stdout.write(f"Created admin: {admin}")

        for _ in range(options["teams"]):
            users = UserWithNameFactory.create_batch(options.get("users"))
            users.append(admin)
            team = TeamWithTaggedUsersFactory.create(members=users)
            self.stdout.write(f"Created {team=} with {users=}")

        checklist = ChecklistDefinitionWithAllTaskTypes(author=admin)
        self.stdout.write(f"Created {checklist=} with tasks")
        recurring_checklist = RecurringChecklist(author=admin, team=team)
        self.stdout.write(f"Created {recurring_checklist=} with tasks")

    def add_arguments(self, parser):
        parser.add_argument(
            "--teams",
            default=DEFAULT_TEAM_COUNT,
            type=int,
            help="The number of teams to create.",
        )
        parser.add_argument(
            "--users",
            default=DEFAULT_USER_COUNT,
            type=int,
            help="The number of users per team to create.",
        )
        parser.add_argument(
            "--seed",
            help="The initial seed to use when generating random data.",
            default="listaflow",
            type=str,
        )
