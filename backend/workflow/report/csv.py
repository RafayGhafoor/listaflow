"""
Handle caching and CSV writing logic for reports
"""

import csv

from workflow.interaction import INTERFACE_TYPE_TO_CLASS
from workflow.interaction.interfaces import BaseNumericInterface
from workflow.report.json import (
    ChecklistDefinitionTrendsJsonReport,
    TemplateRunsJsonReport,
)


class CsvReportBase:
    """
    Base class for CSV report
    """

    delimiter = ","
    csv_fields = []

    def __init__(self, *args, **kwargs):
        self.is_many = kwargs.get("is_many", False)
        self._init_csv_fields()

    def _init_csv_fields(self):
        """
        Initialize CSV fields when creating a report instance
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    def get_csv_fields(self) -> list[str]:
        """
        Get fields (columns) for CSV from attribute
        """
        return self.csv_fields

    def _get_csv_rows(self, data: dict):
        """
        Get data for a singular object
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    def _get_source_data(self):
        """
        Get source data to convert to CSV
        """
        raise NotImplementedError(
            "This method is expected to be implemented in the sub-class."
        )

    def write_csv(self, output, *args, **kwargs):
        """
        Write CSV data to a writeable object
        """
        fieldnames: list[str] = self.get_csv_fields()
        dict_writer = csv.DictWriter(
            output, delimiter=self.delimiter, fieldnames=fieldnames
        )
        dict_writer.writeheader()
        data = self._get_source_data()
        is_many = getattr(self, "is_many", False)
        if is_many:
            for _data in data:
                rows = self._get_csv_rows(_data)
                dict_writer.writerows(rows)
        else:
            rows = self._get_csv_rows(data)
            dict_writer.writerows(rows)


class TemplateRunsCsvReport(TemplateRunsJsonReport, CsvReportBase):
    """
    Generate Recurrence Run CSV report
    """

    def __init__(
        self,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self._init_csv_fields()

    def _init_csv_fields(self):
        # pylint: disable=protected-access
        self.csv_fields = list(self.serializer_class._declared_fields.keys())

    def _get_source_data(self):
        """
        Serialized data as CSV source data
        """
        return self.get_json_data()

    def _get_csv_rows(self, data: list[dict]) -> list[list[dict]]:
        """
        Override to get CSV rows for Run from serialized data

        Args:
            data: dict. Serialized data
        """
        return [data]


class ChecklistDefinitionTrendsCsvReport(
    ChecklistDefinitionTrendsJsonReport, CsvReportBase
):
    """
    Generate Recurrence trend report
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._init_csv_fields()

    def _init_csv_fields(self):
        """
        Get CSV headers for trends report
        """
        self.csv_fields = [
            "Type",
            "Task",
            "Complete On Total",
            "Mean",
            "Median",
            "Mode",
        ]

    def _get_csv_rows(self, data: dict) -> list[dict]:
        """
        Override to generate CSV rows for trend report
        """
        rows: list[dict] = []
        trends = data["trends"]
        for trend in trends:
            row: dict = {}
            task_definition = trend["task_definition"]
            response_rate = trend["response_rate"]
            row["Type"] = task_definition["interface_type"]
            row["Task"] = task_definition["label"]
            completed_count = response_rate["completed_count"]
            total_count = response_rate["total_count"]
            interface_stats = trend["interface_stats"]
            row["Complete On Total"] = f"{completed_count}/{total_count}"
            interface_class = INTERFACE_TYPE_TO_CLASS[task_definition["interface_type"]]
            if interface_stats and issubclass(interface_class, BaseNumericInterface):
                row["Mean"] = interface_stats["mean"]
                row["Median"] = interface_stats["median"]
                row["Mode"] = interface_stats["mode"]
            rows.append(row)
        return rows

    def _get_source_data(self):
        """
        Serialized data as CSV source data
        """
        return self.get_json_data()
