# Generated by Django 3.2.13 on 2022-07-19 20:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("workflow", "0006_auto_20220624_1107"),
    ]

    operations = [
        migrations.AddField(
            model_name="recurrence",
            name="reminder_days",
            field=models.IntegerField(
                blank=True,
                default=1,
                help_text="Days before end date a reminder should be sent",
            ),
        ),
        migrations.AddField(
            model_name="run",
            name="reminder_days",
            field=models.IntegerField(
                blank=True,
                default=1,
                help_text="Days before end date a reminder should be sent",
            ),
        ),
    ]
