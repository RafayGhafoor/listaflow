# Generated by Django 3.2.13 on 2022-05-31 21:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("workflow", "0002_recurrence_run"),
    ]

    operations = [
        migrations.AddField(
            model_name="checklisttask",
            name="response",
            field=models.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name="checklisttaskdefinition",
            name="customization_args",
            field=models.JSONField(blank=True, default=dict),
        ),
        migrations.AddField(
            model_name="checklisttaskdefinition",
            name="interface_type",
            field=models.CharField(
                choices=[
                    ("checkbox", "Checkbox"),
                    ("markdown_textarea", "Markdown textarea"),
                    ("linear_scale_rating", "Linear scale rating"),
                ],
                default="checkbox",
                max_length=50,
            ),
        ),
    ]
