"""
Views for the workflow module.
"""
# Create your views here.
from typing import Any, List, Optional, Union

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.db.models import QuerySet, Q
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.dateparse import parse_date
from django.utils.translation import gettext_lazy as _

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_condition import Or
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, GenericViewSet
from rest_framework_bulk import mixins as bulk_mixins, serializers as bulk_serializers
from lib.permissions import IsSafeMethod

from profiles.models import TaggedMembership, Team, TeamMembership
from utils.exceptions import raise_rest_validation_error
from workflow.filters import ChecklistFilter
from workflow.enums import ChecklistListName
from workflow.interaction import SUBSECTION_INTERFACE
from workflow.models import (
    Checklist,
    ChecklistDefinition,
    ChecklistTask,
    ChecklistTaskDefinition,
    EventRecurrenceMapping,
    Run,
)
from workflow.report import (
    ACCEPTABLE_REPORT_FORMATS,
    REPORT_FORMAT_CSV,
    REPORT_FORMAT_JSON,
    REPORT_FORMAT_PDF,
    REPORT_FORMAT_QUERY_KEY,
)
from workflow.report.csv import (
    ChecklistDefinitionTrendsCsvReport,
    TemplateRunsCsvReport,
)
from workflow.report.json import (
    ChecklistDefinitionTrendsJsonReport,
    RunOverviewJsonReport,
    TemplateRunsJsonReport,
)
from workflow.report.pdf import (
    ChecklistDefinitionTrendsPdfReport,
    TemplateRunsPdfReport,
)
from workflow.serializers import (
    ChecklistActiveArchiveCountSerializer,
    ChecklistReviewSerializer,
    ChecklistDefinitionSerializer,
    ChecklistDefinitionTrendsSerializer,
    ChecklistSerializer,
    ChecklistTaskDefinitionSerializer,
    ChecklistTaskSerializerNested,
    ChecklistDefinitionListSerializer,
    RunReviewSerializer,
    SprintChecklistSerializer,
)
from workflow.permissions import IsAuthor, IsUser, IsAssignee, IsOnTheTeam
from workflow.tasks import create_run_for_team


SWAGGER_REPORT_FILTER_PARAMS = [
    openapi.Parameter(
        "id",
        openapi.IN_PATH,
        type=openapi.TYPE_STRING,
        description="Checklist definition id",
    ),
    openapi.Parameter(
        REPORT_FORMAT_QUERY_KEY,
        openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
        description="Report format, accept "
        f"{', '.join(ACCEPTABLE_REPORT_FORMATS)} only",
    ),
    openapi.Parameter(
        "team_ids",
        openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
        description="Team id",
    ),
    openapi.Parameter(
        "start_date",
        openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
        description="Get trends for runs after start date",
    ),
    openapi.Parameter(
        "end_date",
        openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
        description="Get trends for runs before end date",
    ),
    openapi.Parameter(
        "tags",
        openapi.IN_QUERY,
        type=openapi.TYPE_STRING,
        description="Filter tasks by these tags",
    ),
]


class ReportViewMixin:  # pylint: disable=R0903
    """
    View Mixin for report
    """

    def get_report_format(self, request, *args, **kwargs):  # pylint: disable=R0201
        """
        Get report format from request, raise exception
        if querystring is invalid

        Raises:
            ValidationError: Validation error raised if format
                is not in acceptable list
        """
        not_supported_formats = kwargs.pop("not_supported_formats", set())
        supported_formats_map = ACCEPTABLE_REPORT_FORMATS - not_supported_formats

        report_format = request.GET.get(REPORT_FORMAT_QUERY_KEY, REPORT_FORMAT_JSON)
        if report_format not in supported_formats_map:
            raise_rest_validation_error(
                REPORT_FORMAT_QUERY_KEY,
                _("Report format {report_format} is not accepted").format(
                    report_format=report_format
                ),
            )
        return report_format

    @staticmethod
    def get_csv_report_repsonse(filename: str, writer: Any) -> HttpResponse:
        """
        Get CSV report response

        Args:
            filename: str. File name with extension
            writer: Any. With write_csv method implemented
        """
        headers = {"Content-Disposition": f"attachment; filename={filename}"}
        response = HttpResponse(headers=headers, content_type="text/csv")
        writer.write_csv(response)
        return response

    @staticmethod
    def get_pdf_report_repsonse(filename: str, writer: Any) -> HttpResponse:
        """
        Get PDF report response

        Args:
            filename: str. File name with extension
            writer: Any. With write_pdf method implemented
        """
        headers = {"Content-Disposition": f"attachment; filename={filename}"}
        response = HttpResponse(headers=headers, content_type="application/pdf")
        writer.write_pdf(response)
        return response


class ChecklistDefinitionMixin:  # pylint: disable=too-few-public-methods
    """
    View mixin for Checklist definition views
    """

    def get_queryset(self):
        """
        Build the query set for the views.

        Returns the templates with the same team of the user
        and the global templates (team with null).
        """

        teams = self.request.user.teams.all()
        queryset = ChecklistDefinition.objects.filter(
            Q(active=True) & (Q(team__in=teams) | Q(team__isnull=True))
        )
        return queryset


class ChecklistDefinitionListViewSet(
    ChecklistDefinitionMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """
    Checklist definition list viewset with only name and id
    """

    permission_classes = [IsAuthenticated]
    serializer_class = ChecklistDefinitionListSerializer
    pagination_class = None


class OneOffCheclistDefinitionViewSet(
    ChecklistDefinitionMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """
    One-offs checklist definition list viewset
    """

    permission_classes = [IsAuthenticated]
    serializer_class = ChecklistDefinitionSerializer

    def get_queryset(self):
        query_set = super().get_queryset()
        return query_set.filter(can_create_one_off=True)


class ChecklistDefinitionViewSet(
    ChecklistDefinitionMixin, ModelViewSet, ReportViewMixin
):
    """
    List, create, update, and retrieve checklist definitions.
    """

    serializer_class = ChecklistDefinitionSerializer
    permission_classes = [IsAuthenticated, Or(IsAuthor, IsSafeMethod)]
    lookup_field = "id"

    def get_serializer_context(self):
        """
        build context obj for serializers from request query_params
        """
        query_params = self.request.query_params
        context = {
            "start_date": query_params.get("start_date", ""),
            "end_date": query_params.get("end_date", ""),
            "team_ids": query_params.getlist("team_ids", []),
            "tags": query_params.getlist("tags", []),
        }
        return context

    def perform_create(self, serializer):
        """
        Creates a new checklist definition.
        """
        return serializer.save(author=self.request.user)

    def perform_destroy(
        self: QuerySet[ChecklistTaskDefinition], instance: ChecklistDefinition
    ) -> None:
        """
        Destroy a checklist definition, deactivating if existing checklists use it.
        """
        if instance.checklists.exists():
            instance.active = False
            instance.save()
            return
        instance.delete()

    @staticmethod
    def get_allowed_team_ids(
        current_user: AbstractBaseUser,
        requested_team_ids: Optional[Union[str, List[str]]],
        filter_by_team_ids: Optional[List[str]] = None,
    ) -> List[str]:
        """
        Get current user's teams if no team_ids are passed.
        Else check whether the current user has access to passed
        team_ids and return.
        """
        if filter_by_team_ids is not None:
            teams_query = current_user.teams.filter(id__in=filter_by_team_ids)
        else:
            teams_query = current_user.teams.all()
        user_team_ids = list(teams_query.values_list("id", flat=True))

        if not requested_team_ids:
            if not user_team_ids:
                raise_rest_validation_error("detail", _("User not part of any team"))
            return user_team_ids

        if isinstance(requested_team_ids, list):
            id_list = requested_team_ids
        else:
            id_list = [requested_team_ids]

        unauthorized_access = set(id_list) - set(user_team_ids)
        if unauthorized_access and not current_user.is_superuser:
            raise_rest_validation_error("detail", _("Unauthorized access"))
        return id_list

    @staticmethod
    def filter_runs(context, checklists: Optional[List[Checklist]] = None):
        """
        get runs based on query_params
        """
        runs = Run.objects
        start_date = parse_date(context.get("start_date", ""))
        end_date = parse_date(context.get("end_date", ""))
        runs = runs.filter(team__id__in=context.get("team_ids", []))
        if start_date:
            runs = runs.filter(start_date__date__gte=start_date)
        if end_date:
            runs = runs.filter(start_date__date__lte=end_date)
        if checklists is not None:
            runs = runs.filter(checklists__in=checklists).distinct()
        return runs.all()

    @staticmethod
    def related_checklist_tasks(instance, runs, users):
        """
        fetch related tasks for ChecklistDefinition
        """
        filtered_tasks_query = ChecklistTask.objects.filter(
            checklist__definition__id=instance.id,
            checklist__run__in=runs,
            definition__active=True,
            checklist__assignee__in=users,
        ).exclude(definition__interface_type=SUBSECTION_INTERFACE)
        tasks = filtered_tasks_query.values(
            "definition__label",
            "id",
            "definition__id",
            "response",
            "checklist__assignee__username",
            "checklist__assignee__display_name",
            "definition__interface_type",
            "definition__customization_args",
            "checklist__run__start_date",
            "checklist__run__end_date",
        )
        return tasks.all()

    @staticmethod
    def get_report_info(instance, context):
        """
        get addtional info for pdf report
        """
        team_names = (
            Team.objects.filter(pk__in=context.get("team_ids", [])).all().values("name")
        )
        info = {
            "name": instance.name,
            "team_names": team_names,
            "start_date": context.get("start_date"),
            "end_date": context.get("end_date"),
            "tags": context.get("tags"),
        }
        return info

    @swagger_auto_schema(
        manual_parameters=SWAGGER_REPORT_FILTER_PARAMS,
        responses={
            200: openapi.Response(
                "ChecklistDefinition Trend report", ChecklistDefinitionTrendsSerializer
            )
        },
    )
    @action(methods=["get"], detail=True)
    def trends(self, request, *args, **kwargs):  # pylint: disable=C0103,W0622,W0613
        """
        Serve trends information of a Recurrence
        """
        instance = self.get_object()
        report_format = self.get_report_format(request)
        context = self.get_serializer_context()
        context["team_ids"] = self.get_allowed_team_ids(
            request.user, context.get("team_ids")
        )
        context["runs"] = self.filter_runs(context, instance.checklists.all())
        context["users"] = TeamMembership.get_users_by_tags(
            context["team_ids"], context.get("tags")
        )
        if report_format == REPORT_FORMAT_CSV:
            report = ChecklistDefinitionTrendsCsvReport(
                instance=instance, context=context
            )
            filename = f"report_trends_{instance.id}.csv"
            return self.get_csv_report_repsonse(filename, report)
        if report_format == REPORT_FORMAT_PDF:
            filename = f"report_trends_{instance.id}.pdf"
            info = self.get_report_info(instance, context)
            report = ChecklistDefinitionTrendsPdfReport(
                instance=instance, context=context, info=info
            )
            return self.get_pdf_report_repsonse(filename, report)
        report = ChecklistDefinitionTrendsJsonReport(instance=instance, context=context)
        return Response(report.get_json_data())

    @swagger_auto_schema(manual_parameters=SWAGGER_REPORT_FILTER_PARAMS)
    @action(methods=["get"], detail=True)
    def compare(self, request, *args, **kwargs):
        """
        Return checklists in a given time range for comparison purpose
        """
        instance = self.get_object()
        context = self.get_serializer_context()
        context["team_ids"] = self.get_allowed_team_ids(
            request.user, context.get("team_ids")
        )
        tasks = self.related_checklist_tasks(
            instance,
            self.filter_runs(context),
            TeamMembership.get_users_by_tags(context["team_ids"], context.get("tags")),
        )
        report_format = self.get_report_format(request)
        if report_format == REPORT_FORMAT_CSV:
            report = TemplateRunsCsvReport(instance=tasks, is_many=True)
            filename: str = f"runs_{instance.id}_compare.csv"
            return self.get_csv_report_repsonse(filename, report)
        if report_format == REPORT_FORMAT_PDF:
            filename = f"runs_{instance.id}_compare.pdf"
            info = self.get_report_info(instance, context)
            report = TemplateRunsPdfReport(
                instance=tasks,
                is_many=True,
                info=info,
            )
            return self.get_pdf_report_repsonse(filename, report)
        report = TemplateRunsJsonReport(instance=tasks, is_many=True)
        data = report.get_json_data()
        processed_data = report.group_tasks_by_definition_id(data)
        return Response(processed_data)

    @swagger_auto_schema(manual_parameters=SWAGGER_REPORT_FILTER_PARAMS)
    @action(methods=["get"], detail=True)
    def overview(self, request, *args, **kwargs):
        """
        Return overview of checklists in a given time range
        """
        instance = self.get_object()
        context = self.get_serializer_context()
        context["team_ids"] = self.get_allowed_team_ids(
            request.user, context.get("team_ids")
        )
        runs = self.filter_runs(context, instance.checklists.all())
        context["users"] = TeamMembership.get_users_by_tags(
            context["team_ids"], context.get("tags")
        )
        report = RunOverviewJsonReport(instance=runs, context=context, is_many=True)
        return Response(report.get_json_data())

    @swagger_auto_schema(manual_parameters=[SWAGGER_REPORT_FILTER_PARAMS[0]])
    @action(methods=["get"], detail=True)
    def related_tags(self, request, *args, **kwargs):
        """
        Return related tags for given checklist definition.
        """
        instance = self.get_object()
        context = self.get_serializer_context()
        checklist_definition_team_ids = instance.related_teams()
        team_ids = self.get_allowed_team_ids(
            request.user, context.get("team_ids"), checklist_definition_team_ids
        )
        return Response(TaggedMembership.get_distinct_tags_for_teams(team_ids))


class ChecklistTaskDefinitionViewSet(ModelViewSet):
    """
    List, create, update, and retrieve task definitions in a checklist definition.
    """

    serializer_class = ChecklistTaskDefinitionSerializer
    permission_classes = [IsAuthenticated, Or(IsAuthor, IsSafeMethod)]
    lookup_field = "id"
    pagination_class = None

    def get_object(self):
        if "id" in self.kwargs:
            # Looking for a specific task definition.
            result = get_object_or_404(
                ChecklistTaskDefinition,
                id=self.kwargs["id"],
                active=True,
                checklist_definition_id=self.kwargs["checklist_definition_id"],
            )
        # Getting all task definitions for a checklist definition.
        else:
            result = get_object_or_404(
                ChecklistDefinition,
                id=self.kwargs["checklist_definition_id"],
                active=True,
            )
        self.check_object_permissions(self.request, result)
        return result

    def get_queryset(self):
        """
        Get all the task definitions for this checklist definition.
        """
        if getattr(self, "swagger_fake_view", False):
            return ChecklistTaskDefinition.objects.all()
        return ChecklistTaskDefinition.objects.filter(
            checklist_definition=self.kwargs["checklist_definition_id"],
            active=True,
        )

    def perform_create(self, serializer):
        """
        Creates a task definition for a checklist definition.
        """
        checklist_definition = self.get_object()
        self.check_object_permissions(self.request, checklist_definition)
        serializer.save(checklist_definition=checklist_definition)

    def perform_destroy(
        self: QuerySet[ChecklistTaskDefinition], instance: ChecklistTaskDefinition
    ) -> None:
        """
        Destroy task definition, preserving as inactive if a task uses it.
        """
        if instance.tasks.exists():
            instance.active = False
            instance.save()
            return
        instance.delete()


class UserChecklistViewSet(ModelViewSet):
    """
    List and create checklists for a particular user.
    """

    permission_classes = [IsAuthenticated, Or(IsUser, IsAssignee, IsOnTheTeam)]
    serializer_class = ChecklistSerializer
    lookup_field = "id"

    def get_object(self):
        """
        Get the user this checklist listing is for.
        """
        if "id" in self.kwargs:
            result = get_object_or_404(
                Checklist,
                Q(id=self.kwargs["id"])
                & (
                    Q(assignee__username=self.kwargs["username"])
                    | Q(team__in=self.request.user.teams.all())
                ),
            )
        else:
            result = get_object_or_404(
                get_user_model(), username=self.kwargs["username"]
            )
        self.check_object_permissions(self.request, result)
        return result

    def get_queryset(self) -> QuerySet[Checklist]:
        """
        Get checklists for user.
        """
        if getattr(self, "swagger_fake_view", False):
            return Checklist.objects.all()
        user = self.get_object()
        self.check_object_permissions(self.request, user)
        return (
            Checklist.objects.filter(
                assignee=user,
                definition__active=True,
            )
            .select_related("run", "assignee", "definition")
            .prefetch_related(
                "run__checklists",
                "run__recurrence",
                "run__recurrence__team",
                "run__recurrence__checklist_definition",
                "run__recurrence__interval_schedule",
                "run__checklists__assignee",
            )
        )

    def create(self, request, *args, **kwargs):
        """
        Create a new checklist
        """
        data = request.data
        assignee = self.get_object()
        team_id = data.get("team", None)
        membership = None
        user_tags = None
        team_instance = None
        if team_id:
            team_instance = Team.objects.get(id=team_id)
            membership = TeamMembership.objects.get(
                team=team_id,
                user=assignee,
            )
        if membership:
            user_tags = membership.tags.all()
        checklist_definition_instance = ChecklistDefinition.objects.get(
            id=data.get("definition"),
        )
        instance, _ = Checklist.create_checklist_and_corresponding_tasks(
            assignee,
            checklist_definition_instance,
            user_tags=user_tags,
            team_instance=team_instance,
        )
        response_data = {
            "id": instance.id,
            "name": instance.name,
        }
        return Response(response_data, status=201)


class ChecklistViewSet(
    bulk_mixins.BulkUpdateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
):
    """
    Checklist view set. Does not allow creation.
    """

    permission_classes = [IsAuthenticated, IsAssignee]
    serializer_class = ChecklistReviewSerializer
    lookup_field = "id"
    filterset_class = ChecklistFilter
    queryset = Checklist.objects.get_queryset()

    def get_queryset(self, **kwargs):
        """
        Override to get list name based on queryset
        """
        queryset = super().get_queryset()
        is_no_filter = kwargs.pop("is_no_filter", False)
        is_sort = kwargs.pop("is_sort", False)
        if is_sort:
            queryset = Checklist.objects.sort_by_run_info()
        if not is_no_filter:
            user = self.request.user
            list_name = self.request.GET.get("list_name", ChecklistListName.ALL.name)
            if list_name not in ChecklistListName.names():
                list_name = ChecklistListName.ALL.name
            if list_name == ChecklistListName.TO_DO.name:
                queryset = queryset.filter(assignee=user, completed=False)
            if list_name == ChecklistListName.ASSIGNED_TO_ME.name:
                queryset = queryset.filter(assignee=user)
        return queryset.select_related(
            "run", "assignee", "definition"
        ).prefetch_related(
            "run__checklists",
            "run__recurrence",
            "run__recurrence__team",
            "run__recurrence__checklist_definition",
            "run__recurrence__interval_schedule",
            "run__checklists__assignee",
        )

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "list_name",
                openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description=f"Accept {', '.join(ChecklistListName.names())}",
            )
        ],
    )
    def list(self, request, *args, **kwargs):
        """
        Override to add parameter and update queryset
        """
        queryset = self.filter_queryset(self.get_queryset(is_sort=True))
        queryset = queryset.all_display_for_user(request.user)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_checklist_count_dict(self, user: AbstractBaseUser, is_archived=False):
        """
        Get count dictionary for checklist
        """
        queryset = (
            self.filter_queryset(self.get_queryset(is_no_filter=True))
            .filter(is_archived=is_archived)
            .distinct()
        )
        assigned_to_me_queryset = queryset.filter(assignee=user)
        to_do_queryset = assigned_to_me_queryset.filter(completed=False)
        all_queryset = queryset
        return {
            "ASSIGNED_TO_ME": assigned_to_me_queryset.all_display_for_user(
                user
            ).count(),
            "TO_DO": to_do_queryset.all_display_for_user(user).count(),
            "ALL": all_queryset.all_display_for_user(user).count(),
        }

    @action(methods=["get"], detail=False, url_path="count")
    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                "list_name",
                openapi.IN_QUERY,
                type=openapi.TYPE_STRING,
                description=f"Accept {', '.join(ChecklistListName.names())}",
            )
        ],
        responses={
            200: openapi.Response(
                "Recurrence Trend report", ChecklistActiveArchiveCountSerializer
            )
        },
    )
    def count(self, request, *args, **kwargs):
        """
        Get count of active and archived checklists
        """
        is_archived_count_dict: dict = self.get_checklist_count_dict(
            request.user, is_archived=True
        )
        active_count_dict: dict = self.get_checklist_count_dict(
            request.user, is_archived=False
        )
        data = {
            "active": active_count_dict,
            "archived": is_archived_count_dict,
        }
        serializer = ChecklistActiveArchiveCountSerializer(data=data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data)

    def bulk_update(self, request, *args, **kwargs):
        """
        Override to check user permission on each object updated
        """
        partial = kwargs.pop("partial", False)

        queryset = self.filter_queryset(self.get_queryset())
        # restrict the update to the filtered queryset
        serializer = self.get_serializer(
            queryset,
            data=request.data,
            many=True,
            partial=partial,
        )
        serializer.is_valid(raise_exception=True)
        validated_data = serializer.validated_data
        ids = [data[self.lookup_field] for data in validated_data]
        instances = queryset.filter(id__in=ids)
        for instance in instances:
            self.check_object_permissions(self.request, instance)

        self.perform_bulk_update(serializer)
        return Response(serializer.data, status=200)

    @swagger_auto_schema(
        request_body=bulk_serializers.BulkListSerializer(
            child=ChecklistReviewSerializer()
        ),
    )
    def partial_bulk_update(self, request, *args, **kwargs):
        """Override bulk update method to add swagger custom serializer"""
        return super().partial_bulk_update(request, *args, **kwargs)


class ChecklistTaskViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet,
    ReportViewMixin,
):
    """
    List checklist tasks.
    """

    serializer_class = ChecklistTaskSerializerNested
    permission_classes = [Or(IsAssignee, IsOnTheTeam)]
    lookup_field = "id"
    pagination_class = None

    def get_permissions(self):
        """
        Get the permissions depending of the action
        """
        if self.action == "partial_update":
            permission_classes = [IsAssignee]
        else:
            permission_classes = self.permission_classes
        return [permission() for permission in permission_classes]

    def get_object(self) -> Checklist:
        """
        Get checklist by ID.
        """
        if "id" in self.kwargs:
            result = ChecklistTask.objects.get(
                id=self.kwargs["id"], checklist_id=self.kwargs["checklist_id"]
            )
            check = result.checklist
        else:
            result = get_object_or_404(Checklist, id=self.kwargs["checklist_id"])
            check = result
        self.check_object_permissions(self.request, check)
        return result

    def get_queryset(self) -> QuerySet[ChecklistTask]:
        """
        Get relevant checklist tasks.
        """
        if getattr(self, "swagger_fake_view", False):
            return ChecklistTask.objects.all()
        checklist = self.get_object()
        self.check_object_permissions(self.request, checklist)
        return checklist.tasks.filter(definition__active=True, parent__isnull=True)

    def get_serializer_context(self):
        context = self.request.query_params.dict()
        return context


class RunViewSet(
    mixins.RetrieveModelMixin,
    GenericViewSet,
):
    """
    Run viewset
    """

    lookup_field: str = "id"
    permission_classes = [IsAuthenticated]
    serializer_class = RunReviewSerializer

    def get_object(self) -> Run:
        """ "
        Get Run object for a given recurrence instance
        """
        run_id = self.kwargs.get(self.lookup_field, None)
        return get_object_or_404(Run, id=run_id)

    @swagger_auto_schema(request_body=SprintChecklistSerializer)
    @action(methods=["post"], detail=False)
    def sprintcraft_webhook(
        self, request, *args, **kwargs
    ):  # pylint: disable=no-self-use
        """
        Update tags and create run based on new sprint data from SprintCraft.
        """
        serializer = SprintChecklistSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        event_data = serializer.validated_data
        event_map = EventRecurrenceMapping.objects.filter(
            name__iexact=event_data["event_name"]
        ).first()
        if not event_map or not event_map.recurrence:
            raise_rest_validation_error(
                "detail",
                _(
                    f"No event with name: {event_data['event_name']}"
                    " OR no recurrence related to this event found"
                ),
            )
        event_map.team.update_member_tags(
            event_data["participants"],
            to_small_case=True,
            tags_for_all=[event_data["cell"]],
        )
        create_run_for_team.apply(
            kwargs={
                "recurrence_id": event_map.recurrence.id,
                "participants": list(event_data["participants"].keys()),
                "start_date": event_data["start_date"],
                "end_date": event_data["end_date"],
                "due_date": event_data["end_date"],
            },
        )
        return Response(serializer.data, status=status.HTTP_201_CREATED)
