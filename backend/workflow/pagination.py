"""
standard pagination class
"""
from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    """
    Basic pagination class with size param
    """

    page_size = 24
    page_size_query_param = "size"
    max_page_size = 100
