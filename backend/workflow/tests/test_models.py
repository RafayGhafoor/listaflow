# pylint: disable=too-many-lines
"""
Tests for Workflow models.
"""
from datetime import timedelta
from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.db.models import Count
from django.utils import timezone

import pytest
from django_celery_beat.models import HOURS, PeriodicTask

from backend.workflow.tests.utils import create_team_and_checklist_definition
from lib.models import Tag
from lib.tests.factories import TagFactory
from profiles.models import TeamMembership
from profiles.tests.factories import UserFactory, TeamFactory
from workflow.models import Checklist, ChecklistTask, Recurrence, Run
from workflow.tests.factories import (
    ChecklistDefinitionFactory,
    ChecklistDefinitionWithAllTaskTypes,
    ChecklistFactory,
    ChecklistTaskDefinitionFactory,
    ChecklistTaskFactory,
    IntervalScheduleFactory,
    RecurrenceFactory,
    RunFactory,
)
from workflow.enums import ChecklistStatus

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_partial_checklist_completion():
    """
    If we didn't complete all tasks, the complete
    function should not update completed flag
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    definition = ChecklistTaskDefinitionFactory(required=True)
    tasks.append(ChecklistTaskFactory(checklist=checklist, definition=definition))
    checklist.refresh_from_db()
    assert checklist.completed is False
    assert checklist.completed_on is None
    tasks[0].completed = True
    tasks[0].save()
    status, _ = checklist.complete()
    assert status is False
    checklist.refresh_from_db()
    assert checklist.completed is False


def test_full_checklist_completion():
    """
    If all tasks are completed, the complete function should update completed flag.
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    for task in tasks:
        task.completed = True
        task.save()
    status, _ = checklist.complete()
    assert status is True
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None


def test_required_checklist_completion():
    """
    If all required tasks are completed, complete function should update completed flag.
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    # atleast one required task
    definition = ChecklistTaskDefinitionFactory(required=True)
    tasks.append(ChecklistTaskFactory(checklist=checklist, definition=definition))
    for task in tasks:
        task.definition.active = True
        if task.required:
            task.completed = True
            task.save()
    status, _ = checklist.complete()
    assert status is True
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None


def test_force_checklist_completion():
    """
    Even if all tasks are not completed, the complete function should update
    completed flag if forced.
    """
    checklist = ChecklistFactory()
    tasks = [ChecklistTaskFactory(checklist=checklist) for _ in range(3)]
    for task in tasks:
        task.completed = False
        task.save()
    status, _ = checklist.complete(force=True)
    assert status is True
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None


def test_rollback_completion():
    """
    If a required task is unchecked, remove completion settings from checklist.
    """
    definition = ChecklistTaskDefinitionFactory(required=True)
    checklist = ChecklistFactory(completed=True, completed_on=timezone.now())
    checklist.refresh_from_db()
    assert checklist.completed is True
    assert checklist.completed_on is not None
    ChecklistTaskFactory(definition=definition, checklist=checklist)
    checklist.refresh_from_db()
    assert checklist.completed is False
    assert checklist.completed_on is None


def test_task_completed_on():
    """
    Test setting the completed attribute on a task also marks the completed_on
    attribute.
    """
    task = ChecklistTaskFactory()
    assert task.completed_on is None
    task.completed = True
    task.save()
    task.refresh_from_db()
    assert task.completed_on is not None


def test_task_unchecked_removes_completed_on():
    """
    Test that unchecking a task also removes its completed_on stamp.
    """
    task = ChecklistTaskFactory(completed=True)
    task.refresh_from_db()
    assert task.completed_on is not None
    assert task.completed is True
    task.completed = False
    task.save()
    assert task.completed_on is None
    assert task.completed is False


def check_empty():
    """
    Assert that the database doesn't have any checklists or checklist tasks.
    """
    checklist_models = Checklist.objects.all()
    checklist_task_models = ChecklistTask.objects.all()
    assert len(checklist_models) == 0
    assert len(checklist_task_models) == 0


def test_create_checklist_and_corresponding_tasks_creates_checklist_and_tasks():
    """Test calling Checklist.create_checklist_and_corresponding_tasks creates
    checklist and task models.
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee, checklist_definition
    )

    # End verification.
    checklist_models = Checklist.objects.all()
    checklist_task_models = list(ChecklistTask.objects.all())
    assert checklist_models.get() == checklist
    assert sorted(checklist_task_models) == sorted(tasks)


def test_create_checklist_and_corresponding_skips_nonmatching_tasks():
    """Test calling Checklist.create_checklist_and_corresponding_tasks skips tasks that
    don't match tag specifications.
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["test"],
    )

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=[TagFactory()],
    )

    # End verification.
    check_empty()
    assert checklist is None
    assert not tasks


def test_create_checklist_creates_tasks_for_match():
    """Test calling Checklist.create_checklist_and_corresponding_tasks creates
    checklists when the tags match
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["test", "thing"],
    )

    # Action.
    checklist, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=Tag.objects.all(),
    )

    # End verification.
    checklist_models = Checklist.objects.all()
    checklist_task_models = list(ChecklistTask.objects.all())
    assert checklist_models.get() == checklist
    assert sorted(checklist_task_models) == sorted(tasks)
    assert sorted(list(tasks[0].tags.all())) == sorted(list(Tag.objects.all()))


def test_create_checklist_fails_incomplete_match():
    """Test calling Checklist.create_checklist_and_corresponding_tasks creates
    checklists when the tags match
    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["test", "thing"],
    )

    # Action.
    Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=[Tag.objects.get(name="test")],
    )

    # End verification.
    assert not Checklist.objects.all().exists()


def test_create_checklist_creates_subsections():
    """
    Test calling Checklist.create_checklist_and_corresponding_tasks creates
    parent/child relations for subsections
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    nested_task = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
    )

    # Act
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
    )

    # Assert
    assert len(tasks) == 2
    assert tasks[0].definition == subsection
    assert tasks[1].definition == nested_task
    assert tasks[1].parent == tasks[0]


def test_create_checklist_skips_unmatched_subsections():
    """
    Test calling Checklist.create_checklist_and_corresponding_tasks ignores
    unmatched subsections
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    simple_task = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["foo"],
    )
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        tags=["bar"],
    )
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
    )

    # Act
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=Tag.objects.filter(name="foo"),
    )

    # Assert
    assert len(tasks) == 1
    assert tasks[0].definition == simple_task


def test_create_checklist_skips_unmatched_subsection_children():
    """
    Test calling Checklist.create_checklist_and_corresponding_tasks ignores
    unmatched subsection children
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    nested_task = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
        tags=["foo"],
    )
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
        tags=["bar"],
    )

    # Act
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
        user_tags=Tag.objects.filter(name="foo"),
    )

    # Assert
    assert len(tasks) == 2
    assert tasks[0].definition == subsection
    assert tasks[1].definition == nested_task


def test_create_checklist_skips_subsection_if_children_unmatched():
    """
    Test calling Checklist.create_checklist_and_corresponding_tasks skips
    subsection with unmatched children
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    simple_task = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
        tags=["foo"],
    )
    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
        parent=subsection,
        tags=["bar"],
    )

    # Act
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
    )

    # Assert
    assert len(tasks) == 1
    assert tasks[0].definition == simple_task


@pytest.mark.parametrize(
    "due_days_no,reminders", [[1, [1, 2]], [2, [0, 1]], [3, [3, 4]]]
)
def test_create_run_for_team_creates_run_model(due_days_no, reminders):
    """
    Test calling Run.create_run_for_team method creates Run model with correct
    values
    """
    # Baseline verification.
    models = Run.objects.all()
    assert len(models) == 0

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence = RecurrenceFactory.create(
        team=team,
        checklist_definition=checklist_definition,
        due_days_no=due_days_no,
        reminders=reminders,
    )

    # Action.
    recurrence.create_run()

    # End verification.
    run = Run.objects.first()
    due_date = run.due_date
    expected_due_date = timezone.now().date() + timezone.timedelta(due_days_no)
    assert due_date == expected_due_date
    assert run.reminders == reminders


@patch("workflow.models.Checklist.create_checklist_and_corresponding_tasks")
def test_create_run_for_team_calls_create_checklist_and_corresponding_tasks(
    mock_verify,
):
    """Test calling Run.create_run_for_team method calls
    Checklist.create_checklist_and_corresponding_tasks.
    """
    # Baseline verification.
    assert not mock_verify.called

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    mock_verify.return_value = ChecklistFactory(), [ChecklistTaskFactory()]

    # Action.
    Run.create_run_for_team(team, checklist_definition)

    # End verification.
    assert mock_verify.called


def test_creating_recurrence_model_creates_periodic_task_model():
    """Test to ensure creating a recurrence model creates a PeriodicTask model."""
    # Baseline verification.
    models = PeriodicTask.objects.all()
    assert len(models) == 0

    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()

    # Action.
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1
    assert periodic_task_models[0].interval.id == recurrence_model.interval_schedule.id


def test_creating_recurrence_interval_schedule_model_sets_last_run_at():
    """Test to ensure creating a recurrence model with interval sets last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now() + timezone.timedelta(hours=1)

    # Action.
    Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )

    # verification.
    periodic_task_model = PeriodicTask.objects.first()
    actual_start_time = timezone.now() + timezone.timedelta(
        seconds=periodic_task_model.schedule.is_due(timezone.now()).next
    )
    allowed_delta = timezone.timedelta(seconds=1)
    assert abs(actual_start_time - start_date) < allowed_delta


def test_creating_recurrence_interval_schedule_model_with_past_start_date():
    """Test to ensure creating a recurrence model with past start date raises"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now() - timezone.timedelta(hours=1)

    # Action.
    with pytest.raises(ValidationError, match="Start date cannot be set in past"):
        Recurrence.objects.create(
            start_date=start_date,
            interval_schedule=interval,
            team=team,
            checklist_definition=checklist_definition,
        )


def test_updating_recurrence_interval_schedule_does_change_last_run_at():
    """Test to ensure updating an interval_schedule does change last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now()

    recurrence = Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )
    last_run_at = recurrence.periodic_task.last_run_at
    interval_new = IntervalScheduleFactory(every=30)
    recurrence.interval_schedule = interval_new
    recurrence.save()

    # verify
    assert recurrence.periodic_task.last_run_at != last_run_at


def test_updating_recurrence_team__model_does_not_change_last_run_at():
    """Test to ensure updating a recurrence model team does not change last_run_at"""
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    interval = IntervalScheduleFactory(period=HOURS)
    start_date = timezone.now()

    recurrence = Recurrence.objects.create(
        start_date=start_date,
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )
    last_run_at = recurrence.periodic_task.last_run_at
    new_team, _, _ = create_team_and_checklist_definition(username_prefix="new")
    recurrence.team = new_team
    recurrence.save()

    # verify
    assert recurrence.periodic_task.last_run_at == last_run_at


def test_creating_recurrence_model_adds_correct_task_in_creates_periodic_task_model():
    """Test to ensure creating a recurrence model adds correct task in the newly created
    PeriodicTask model.
    """
    # Setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()

    # Action.
    RecurrenceFactory.create(team=team, checklist_definition=checklist_definition)

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1
    assert periodic_task_models[0].task == "workflow.tasks.create_checklist_for_users"


def test_updating_a_recurrence_model_does_not_creates_new_periodic_task_model():
    """Test to ensure updating a recurrence model does not creates new
    PeriodicTask model.
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Baseline verification.
    models = PeriodicTask.objects.all()
    assert len(models) == 1

    # Action.
    recurrence_model.checklist_definition = ChecklistDefinitionFactory()
    recurrence_model.save()

    # End verification.
    periodic_task_models = PeriodicTask.objects.all()
    assert len(periodic_task_models) == 1


def test_deactivating_recurrence_model_disables_periodic_task_model():
    """Test to ensure deactivating a recurrence model disables PeriodicTask
    model.
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )

    # Baseline verification.
    assert recurrence_model.active
    assert recurrence_model.periodic_task.enabled

    # Action.
    recurrence_model.active = False
    recurrence_model.save()

    # End verification.
    recurrence_model.refresh_from_db()
    assert not recurrence_model.active
    assert not recurrence_model.periodic_task.enabled


def test_checklist_get_all_display_for_user():
    """Test get all checklist for displaying purpose of a given user"""
    # Setup
    team1, definition1, _task_definitions = create_team_and_checklist_definition(
        username_prefix="team1",
        team_member_count=3,
    )
    team2, definition2, _task_definitions = create_team_and_checklist_definition(
        username_prefix="team2",
        team_member_count=4,
    )

    # Add team2's member to team1
    user_team1 = team1.members.first()
    user_team2 = team2.members.first()
    team1.members.add(user_team2)
    team1.save()

    # One off
    ChecklistFactory.create(assignee=user_team1)
    ChecklistFactory.create(assignee=user_team2)

    # Run checklists
    Run.create_run_for_team(team1, definition1)
    Run.create_run_for_team(team2, definition2)

    queryset = Checklist.objects.get_queryset()

    # Action
    # Checklists of other team not included
    # user in 1 team, 1 one-off + 1 recurring
    assert queryset.all_display_for_user(user_team1).count() == 2
    # user in 2 teams, 1 one-off + 2 recurring
    assert queryset.all_display_for_user(user_team2).count() == 3


def test_sort_by_run_due_date_for_checklist():
    """Test sort by run_due_date for checklist"""
    team, definition, _task_definitions = create_team_and_checklist_definition()
    oldest_run, middle_run, latest_run = tuple(
        Run.create_run_for_team(team, definition) for _ in range(3)
    )
    now_dt = timezone.now().date()

    oldest_run.due_date = now_dt - timedelta(days=1)
    oldest_run.save()

    middle_run.due_date = now_dt
    middle_run.save()

    latest_run.due_date = now_dt + timedelta(days=1)
    latest_run.save()

    queryset = Checklist.objects.sort_by_run_info()
    first_cl = queryset.first()
    last_cl = queryset.last()
    assert first_cl.due_date == oldest_run.due_date
    assert last_cl.due_date == latest_run.due_date


def test_run_stats():
    """
    Test get stats of Run
    """
    (
        team,
        checklist_definition,
        _test_definitions,
    ) = create_team_and_checklist_definition()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    run = Run.create_run_for_team(team, checklist_definition)

    checklists = run.checklists.all()
    assert len(checklists) == 2


def test_run_label():
    """
    Test run instance __str__ representation
    """
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)

    # test with no recurrence instance, start and end date should be now
    run_instance = Run.create_run_for_team(team, checklist_definition)
    expected_label = "Run, <checklist definition:" f" {checklist_definition}>"
    assert str(run_instance) == expected_label

    # test with recurrence instance, end date should be start of next run
    interval = IntervalScheduleFactory(period=HOURS)
    recurrence = Recurrence.objects.create(
        interval_schedule=interval,
        team=team,
        checklist_definition=checklist_definition,
    )

    run_instance = recurrence.create_run()
    expected_label = (
        f"Run for {recurrence.id}, <checklist definition:" f" {checklist_definition}>"
    )
    assert str(run_instance) == expected_label


def test_checklist_task_trends():
    """
    Test checklist task trend
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        task_definitions,
    ) = create_team_and_checklist_definition()
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition
    )
    runs = [recurrence_model.create_run()]

    # Action
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1
    assert len(checklist_definition.related_runs()) == 1

    ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition, active=True
    )
    runs.append(recurrence_model.create_run())
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 2
    assert len(checklist_definition.related_runs()) == 2

    task_definitions[0].active = False
    task_definitions[0].save()
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1

    checklist_definition.refresh_from_db()
    assert len(checklist_definition.all_tasks_trends(runs, team.members.all())) == 1


def test_recurrence_run_constrained_by_tags():
    """
    Test that the 'tags' field on a Recurrence is used to filter team members.
    """
    # Baseline setup.
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition(team_member_count=4)
    user1, user2, user3, _user4 = team.members.all()
    user1_membership = TeamMembership.objects.get(user=user1)
    user1_membership.tags.set(["test", "test2", "test3"])
    user2_membership = TeamMembership.objects.get(user=user2)
    user2_membership.tags.set(["test", "test2"])
    user3_membership = TeamMembership.objects.get(user=user3)
    user3_membership.tags.set(["test"])
    recurrence_model = RecurrenceFactory.create(
        team=team, checklist_definition=checklist_definition, tags=["test", "test2"]
    )
    recurrence_model.create_run()

    # There should be only one, as only one has all matching tags.
    checklists = Checklist.objects.all()
    assert checklists.count() == 2
    assert checklists.filter(assignee=user1).exists()
    assert checklists.filter(assignee=user2).exists()


def test_subsection_is_incomplete_while_not_all_subtasks_are_complete():
    """
    Test subsection completeness works correctly while not all subtasks are complete
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    ChecklistTaskDefinitionFactory.create_batch(
        2,
        checklist_definition=checklist_definition,
        parent=subsection,
    )
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
    )

    # Act
    tasks[1].completed = True
    tasks[1].save()

    # Assert
    assert len(tasks) == 3
    assert tasks[1].parent == tasks[0]
    assert not tasks[0].completed


def test_subsection_is_complete_when_all_subtasks_are_complete():
    """
    Test subsection completeness works correctly when all subtasks are complete
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    ChecklistTaskDefinitionFactory.create_batch(
        2,
        checklist_definition=checklist_definition,
        parent=subsection,
    )
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
    )

    # Act
    tasks[1].completed = True
    tasks[1].save()
    tasks[2].completed = True
    tasks[2].save()

    # Assert
    assert len(tasks) == 3
    assert tasks[1].parent == tasks[0]
    assert tasks[2].parent == tasks[0]
    assert tasks[0].completed


def test_complete_subsection_becomes_incomplete_when_subtask_is_incomplete():
    """
    Test subsection completeness works correctly when a subtask becomes incomplete
    """
    # Baseline verification.
    check_empty()

    # Arrange
    checklist_definition = ChecklistDefinitionFactory()
    assignee = UserFactory()
    subsection = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition,
    )
    ChecklistTaskDefinitionFactory.create_batch(
        2,
        checklist_definition=checklist_definition,
        parent=subsection,
    )
    _, tasks = Checklist.create_checklist_and_corresponding_tasks(
        assignee,
        checklist_definition,
    )

    # Act
    tasks[1].completed = True
    tasks[1].save()
    tasks[2].completed = True
    tasks[2].save()
    tasks[1].completed = False
    tasks[1].save()

    # Assert
    assert len(tasks) == 3
    assert tasks[1].parent == tasks[0]
    assert tasks[2].parent == tasks[0]
    assert not tasks[0].completed


def test_checklist_task_definition_ordering():
    """
    Test ChecklistTaskDefinition ordering based on the `OrderedModel` `order` field

    """
    # Baseline verification.
    check_empty()

    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    initial_order = [
        ChecklistTaskDefinitionFactory.create(
            checklist_definition=checklist_definition, order=i
        )
        for i in range(3)
    ]

    # Get Task Definitions and check ordering matches initial order set
    task_definitions = checklist_definition.task_definitions.all()

    assert task_definitions[0] == initial_order[0]
    assert task_definitions[1] == initial_order[1]
    assert task_definitions[2] == initial_order[2]

    # Change Ordering
    task_definitions[2].order = 0
    task_definitions[2].save()
    task_definitions[1].order = 1
    task_definitions[1].save()
    task_definitions[0].order = 2
    task_definitions[0].save()

    # Get Task Definitions and check ordering matches new order set
    task_definitions = checklist_definition.task_definitions.all()

    assert task_definitions[0] == initial_order[2]
    assert task_definitions[1] == initial_order[1]
    assert task_definitions[2] == initial_order[0]


def test_checklist_task_definition_label():
    """
    Test ChecklistTaskDefinition __str__ representation
    """
    # Setup.
    checklist_definition = ChecklistDefinitionFactory()
    expected_label = "Test Checklist Task Definition"
    checklist_task_definition = ChecklistTaskDefinitionFactory.create(
        checklist_definition=checklist_definition, label=expected_label
    )

    assert str(checklist_task_definition) == expected_label


def test_checklist_name():
    """
    Test Checklist __str__ representation
    """
    # Setup.
    expected_name = "Test Checklist"
    checklist = ChecklistFactory(name=expected_name)

    assert str(checklist) == expected_name


def test_checklist_status():
    """
    Test Checklist `status` property returns correct status
    """
    # Setup.
    checklist = ChecklistFactory(completed=True)
    tasks = [
        ChecklistTaskFactory(checklist=checklist, completed=True),
        ChecklistTaskFactory(checklist=checklist, completed=True),
    ]

    # Check checklist status completed
    checklist.refresh_from_db()
    assert checklist.status == ChecklistStatus.COMPLETED

    tasks[0].completed = False
    tasks[0].save()

    checklist.completed = False
    checklist.save()

    # Check checklist status in progress
    checklist.refresh_from_db()
    assert checklist.status == ChecklistStatus.IN_PROGRESS

    tasks[1].completed = False
    tasks[1].save()

    # Check checklist status to do
    checklist.refresh_from_db()
    assert checklist.status == ChecklistStatus.TO_DO

    yesterday = timezone.now().date() - timezone.timedelta(days=1)
    team = TeamFactory()
    run = RunFactory(due_date=yesterday, team=team)
    checklist.run = run
    checklist.save()

    # Check checklist status past due
    checklist.refresh_from_db()
    assert checklist.status == ChecklistStatus.PAST_DUE

    today = timezone.now().date()
    checklist.run.end_date = today + timezone.timedelta(days=1)
    checklist.run.due_date = today + timezone.timedelta(days=2)
    checklist.run.save()

    # Check checklist status upcoming
    checklist.refresh_from_db()
    assert checklist.status == ChecklistStatus.UP_COMING


def test_checklist_task_names_with_children():
    """
    Test the checklist model method to return a mapping of parent-child labels.
    """
    # create all types of tasks
    checklist_definition = ChecklistDefinitionWithAllTaskTypes()
    checklist, _ = Checklist.create_checklist_and_corresponding_tasks(
        UserFactory(), checklist_definition
    )
    parent_child_mapping = checklist.task_names_with_children()
    subsections = checklist.tasks.annotate(c=Count("children")).filter(c__gt=0).all()
    for subsection in subsections:
        assert subsection.label in parent_child_mapping
        subtasks = checklist.tasks.filter(parent=subsection).values_list(
            "definition__label", flat=True
        )
        assert list(subtasks) == parent_child_mapping[subsection.label]
