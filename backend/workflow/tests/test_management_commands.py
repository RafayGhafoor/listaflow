"""
Tests for Workflow management commands
"""
from io import StringIO

from django.core.management import call_command

import pytest

from profiles.models import User, Team, TaggedMembership
from workflow.models import (
    Run,
    RunTrendTaskReport,
    ChecklistDefinition,
    ChecklistTaskDefinition,
    Recurrence,
    IntervalSchedule,
)
from workflow.interaction import INTERFACE_TYPE_CHOICES
from workflow.tests.factories import ChecklistTaskDefinitionFactory
from workflow.tests.utils import create_team_and_checklist_definition
from workflow.management.commands.seed_data import (
    DEFAULT_USER_COUNT,
    DEFAULT_TEAM_COUNT,
)

pytestmark = [
    pytest.mark.django_db(transaction=True),
    pytest.mark.usefixtures("celery_worker", "patch_flush_many_cache"),
]


def test_command_create_and_compute_trends_report():
    """Test command creating (if missing) and computing trends report for recurrence"""
    (
        team,
        checklist_definition,
        _task_definitions,
    ) = create_team_and_checklist_definition()
    # One of these is already created by the previous call. Let's add one more.
    ChecklistTaskDefinitionFactory.create(checklist_definition=checklist_definition)
    Run.create_run_for_team(team, checklist_definition)
    assert RunTrendTaskReport.objects.count() == 2

    RunTrendTaskReport.objects.all().delete()
    assert RunTrendTaskReport.objects.count() == 0

    options = {"checklist_definition_id": checklist_definition.id}
    call_command("create_compute_trends_report", **options)
    assert RunTrendTaskReport.objects.count() == 2

    with pytest.raises(SystemExit):
        dummy_options = {"checklist_definition_id": "dummyid0"}
        call_command("create_compute_trends_report", **dummy_options)


def test_seed_data():
    """Test seed data command sets up db correctly"""
    out = StringIO()
    call_command("seed_data", stdout=out)

    assert "Created admin" in out.getvalue()
    assert User.objects.filter(is_superuser=True).count() == 1
    assert "Created team=" in out.getvalue()
    # Users plus admin
    assert User.objects.count() == DEFAULT_USER_COUNT * DEFAULT_TEAM_COUNT + 1
    assert Team.objects.count() == DEFAULT_TEAM_COUNT

    team = Team.objects.first()
    assert team.name.startswith("Team ")

    # Unique tag for each user in team
    tags = TaggedMembership.get_distinct_tags_for_teams([team.id])
    assert tags.count() == DEFAULT_USER_COUNT

    expected_checklists = 2
    assert ChecklistDefinition.objects.count() == expected_checklists
    expected_subtasks = 2
    expected_tasks = (
        len(INTERFACE_TYPE_CHOICES) + expected_subtasks
    ) * expected_checklists
    assert ChecklistTaskDefinition.objects.count() == expected_tasks
    assert Recurrence.objects.count() == 1


def _seed_data_model_counts():
    models = [User, Team, ChecklistDefinition, IntervalSchedule, Recurrence]
    return {str(model): model.objects.count() for model in models}


def test_seed_data_rerun():
    """Ensure that rerunning seed_data does not create extra instances"""

    call_command("seed_data")
    model_count_expected = _seed_data_model_counts()

    call_command("seed_data")
    model_count_result = _seed_data_model_counts()
    assert model_count_result == model_count_expected
