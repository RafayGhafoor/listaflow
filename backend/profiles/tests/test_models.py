"""
Tests for profiles view module.
"""
import pytest
from profiles.models import TeamMembership

from profiles.tests.factories import TeamFactory, UserFactory

pytestmark = [
    pytest.mark.django_db(transaction=True),
]


def test_update_member_tags():
    """Test updating member tags"""
    users = UserFactory.create_batch(5)
    team = TeamFactory()
    team.members.set(users)
    tags = ["tag1", "tag2"]
    new_tags = {user.email: tags for user in users}
    team.update_member_tags(new_tags)
    memberships = TeamMembership.objects.filter(team=team.id).all()
    for membership in memberships:
        assert set(membership.tags.names()) == set(tags)

    # check empty and updated tags
    updated_tags = ["tag1", "tag4", "tag5"]
    updated_tag_map = {users[0].email: updated_tags, users[1].email: []}
    team.update_member_tags(updated_tag_map)
    assert set(memberships.filter(user=users[0].id).first().tags.names()) == set(
        updated_tags
    )
    assert not list(memberships.filter(user=users[1].id).first().tags.names())
