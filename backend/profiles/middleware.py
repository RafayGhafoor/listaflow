"""
custom profile middleware
"""
from django.http.response import JsonResponse

from oauthlib.oauth2.rfc6749.errors import OAuth2Error
from social_core.exceptions import SocialAuthBaseException
from social_django.middleware import SocialAuthExceptionMiddleware


class CustomSocialAuthExceptionMiddleware(SocialAuthExceptionMiddleware):
    """
    returns proper error response on authentication failure instead of server error.
    """

    def get_message(self, _, exception):
        """
        builds message dict with error details
        """
        return {"error": exception.error, "detail": exception.description}

    def process_exception(self, request, exception):
        """
        returns json response with proper message if it is a social exception
        """
        if isinstance(exception, (OAuth2Error, SocialAuthBaseException)):
            return JsonResponse(
                self.get_message(request, exception),
                status=exception.status_code,
            )
        return None
