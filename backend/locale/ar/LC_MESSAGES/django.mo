��          |      �          &   !  )   H     r     y     �  F   �     �  
   �     �     	  '     �  :  N   �  D        d     m     |  Y   �  2   �     !  ,   0     ]  ^   w        	         
                                          A user with this email already exists. A user with this username already exists. Arabic English Name of User Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. User verified successfully Vietnamese email address username {endpoint} is not a valid API Endpoint. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 يوجد بالفعل مستخدم بهذا البريد الإلكتروني. يوجد بالفعل مستخدم باسم المستخدم هذا. عربي إنجليزي اسم المستخدم مطلوب. 150 حرفًا أو أقل. أحرف وأرقام و @ /. / + / - / _ فقط. تم التحقق من المستخدم بنجاح فيتنامي عنوان البريد الالكترونى اسم االمستخدم {endpoint} ليست نقطة نهاية صالحة لواجهة برمجة التطبيقات. 