"""
Django storage backends
"""
from storages.backends.s3boto3 import S3Boto3Storage


class StaticRootS3Boto3Storage(S3Boto3Storage):  # pylint: disable=W0223
    """
    S3 Static storage class
    """

    location = "static"
    default_acl = "public-read"


class MediaRootS3Boto3Storage(S3Boto3Storage):  # pylint: disable=W0223
    """
    S3 media storage class
    """

    location = "media"
    file_overwrite = False
