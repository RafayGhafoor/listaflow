"""
Tests for cache functions
"""
from django.contrib.auth import get_user_model

from utils.cache import CACHE_KEY_DELIMITER, make_cache_key, make_pk_cache_key


def test_make_cache_key():
    """
    Test make_cache_key function generating keys properly
    """
    args = ["dummy", "test", "cache"]
    expected_result = "dummy{d}test{d}cache".format(d=CACHE_KEY_DELIMITER)
    result = make_cache_key(*args)
    assert expected_result == result


def test_make_pk_cache_key():
    """
    Test make cache key based on model information for model instance
    """
    user_model = get_user_model()
    pk = 1
    model = user_model(pk=pk)
    cls_name = model.__class__.__name__.lower()
    pk_name = model._meta.pk.name
    result = make_pk_cache_key(model)
    expected_result = "{class_name}{d}{pk_name}{d}{pk}".format(
        class_name=cls_name, pk_name=pk_name, pk=pk, d=CACHE_KEY_DELIMITER
    )
    assert result == expected_result
