import { ListController } from "@opencraft/providence/base/lists/types/ListController";
import { SingleController } from "@opencraft/providence/base/singles/types/SingleController";
import { useList, useSingle } from "@opencraft/providence/react-plugin";
import { Run, Task, TaskList } from "../types/Task";
import { Paginated } from "../components/Paginated";
import { TaskItem } from "../components/TaskItem";
import { Subsection } from "../components/Subsection";
import { WORKFLOW_API } from "../constants/api-urls";
import Accordion from "react-bootstrap/Accordion";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import { useEffect, useState, useCallback, useMemo } from "react";
import { useParams } from "react-router-dom";
import { Body } from "../components/Body";
import { Title } from "../components/Title";
import { CustomAlert } from "../components/Alerts";
import { LoadSection } from "../components/LoadSection";
import { ProgressState } from "../components/ChecklistProgress";
import useAuth from "../hooks/useAuth";
import { Trans, useTranslation } from "react-i18next";
import { dateString } from "../utils/helpers";
import { ConfettiState } from "../components/Confetti";

type ChecklistDetailRouteParams = {
  checklistId: string;
};

declare interface TaskWithSubtasksControllers {
  taskController: SingleController<Task>;
  subtasksController: ListController<Task>;
}

declare interface TaskWithSubtasksControllersMap {
  [key: string]: TaskWithSubtasksControllers;
}

declare interface TaskWithSubtasksItem {
  taskController: SingleController<Task>;
  apiEndpoint: string;
  updateControllers: Function;
}

declare interface ChecklistState {
  totalTasks: number;
  completedTasks: number;
  requiredCompletedTasks: number;
  requiredTotalTasks: number;
  tasksWithInteraction: string[];
  failed: boolean;
}

function updateChecklistStatePerTask(
  stats: ChecklistState,
  task: SingleController<Task>
) {
  stats.totalTasks += 1;
  if (task.x?.required) {
    stats.requiredTotalTasks += 1;
    if (task.x?.completed) {
      stats.requiredCompletedTasks += 1;
    }
    if (task.p?.response?.errors?.length && !stats.failed) {
      stats.failed = true;
    }
  }
  if (task.x?.completed) {
    stats.completedTasks += 1;
  }
}

const TaskWithSubtasks = ({
  taskController,
  apiEndpoint,
  updateControllers,
}: TaskWithSubtasksItem) => {
  const subtasksController = useList<Task>(
    ["tasks", `${taskController.x!.id}`, "children"],
    { endpoint: apiEndpoint }
  );

  useEffect(() => {
    subtasksController.makeReady(taskController.x!.children);
  }, [subtasksController, taskController.x]);

  const taskIsSubsection = taskController.x!.interface_type === "subsection";

  // List controller don't provide stable refs, so we work around that by serializing data to JSON
  const subtasksJson = JSON.stringify(subtasksController.rawList);
  useEffect(() => {
    updateControllers(taskController, subtasksController);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateControllers, taskController.x, subtasksJson]);

  if (!taskIsSubsection) {
    return <TaskItem controller={taskController} />;
  }

  const subsectionComplete =
    (taskController.x!.required
      ? subtasksController.list
          .filter((sc) => sc.x!.required)
          .filter((sc) => !sc.x!.completed).length
      : subtasksController.list.filter((sc) => !sc.x!.completed).length) === 0;

  return (
    <Subsection controller={taskController} isCompleted={subsectionComplete}>
      {subtasksController.list.map((cc) => (
        <TaskItem controller={cc} key={cc.x!.id} />
      ))}
    </Subsection>
  );
};

const emptyChecklistStats: ChecklistState = {
  totalTasks: 0,
  completedTasks: 0,
  requiredCompletedTasks: 0,
  requiredTotalTasks: 0,
  tasksWithInteraction: [],
  failed: false,
};

export const ChecklistDetail = () => {
  const { t } = useTranslation();
  const { auth } = useAuth();
  const username = auth?.username!;
  const { checklistId } = useParams<ChecklistDetailRouteParams>();
  const checklistUrl = `${WORKFLOW_API}/user/${username}/checklist`;
  const [showForceSubmitAlert, setShowForceSubmitAlert] = useState(false);

  // checklist details
  const checklistController = useSingle<TaskList>([username, checklistId!], {
    endpoint: `${checklistUrl}/${checklistId}/`,
  });

  // track submission count to update submit button text
  const submissionCount = useSingle<{ num: number }>(
    [checklistId!, "submissionCount"],
    {
      endpoint: "#",
      x: { num: 0 },
    }
  );

  /// Read only mode, used for user in the same team to see coworkers checklists
  const readOnly = useSingle<{ value: boolean }>("onlyRead", {
    endpoint: "#",
    x: { value: true },
  });

  useEffect(() => {
    checklistController.get().then(() => {
      // Check if the logged user is the assignee to disable the only read mode
      if (checklistController.x?.assignee.username === auth?.username) {
        readOnly.p.value.model = false;
      }
      // update checklistController on page number change to handle hidden completed tasks
      if (checklistController.x?.completed) {
        submissionCount.p.num.model = 1;
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // tasks list
  const tasksApiEndpoint = `${checklistUrl}/${checklistId}/task/`;
  const tasksController = useList<Task>([checklistId!, "task"], {
    endpoint: tasksApiEndpoint,
    paginated: false,
  });

  useEffect(() => {
    tasksController.get();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [checklistId, username]);

  // progress bar controller
  const progressValue = useSingle<ProgressState>("taskProgress", {
    endpoint: "#",
  });
  // confetti controller
  const confettiState = useSingle<ConfettiState>("confettiState", {
    endpoint: "#",
  });

  const [allControllers, setAllControllers] =
    useState<TaskWithSubtasksControllersMap>({});
  const updateControllers = useCallback(
    (taskController, subtasksController) => {
      const update: TaskWithSubtasksControllersMap = {
        [taskController.x!.id]: {
          taskController,
          subtasksController,
        },
      };
      setAllControllers((prevState: TaskWithSubtasksControllersMap) => {
        return { ...prevState, ...update };
      });
    },
    [setAllControllers]
  );

  const checklistState = useMemo(() => {
    const result = Object.assign({}, emptyChecklistStats);
    Object.values(allControllers).forEach(
      ({ taskController, subtasksController }: TaskWithSubtasksControllers) => {
        const taskIsSubsection =
          taskController.x!.interface_type === "subsection";
        if (taskIsSubsection) {
          result.tasksWithInteraction.push(taskController.x!.id);
          subtasksController.list.forEach((sub) =>
            updateChecklistStatePerTask(result, sub)
          );
        } else {
          if (taskController.x?.interface_type !== "checkbox") {
            result.tasksWithInteraction.push(taskController.x!.id);
          }
          updateChecklistStatePerTask(result, taskController);
        }
      }
    );
    return result;
  }, [allControllers]);

  if (checklistState.requiredTotalTasks) {
    progressValue.p.val.model =
      (checklistState.requiredCompletedTasks /
        checklistState.requiredTotalTasks) *
      100;
  }

  const pendingTasks = checklistState.requiredTotalTasks
    ? checklistState.requiredTotalTasks - checklistState.requiredCompletedTasks
    : -1;

  const items_text =
    pendingTasks === 1
      ? t("checklistDetail.itemSingular")
      : t("checklistDetail.itemPlural");

  const submitChecklist = (force = false) => {
    if (pendingTasks > 0 && !force) {
      setShowForceSubmitAlert(true);
      return;
    }
    checklistController.p.completed.errors = [];
    checklistController
      .patch({ completed: true, force_complete: force })
      .then((x) => {
        if (x.completed) {
          submissionCount.p.num.model += 1;
          confettiState.p.fire.model = Math.random();
        }
        checklistController.setX(x);
      })
      .catch((err) => {
        checklistController.p.completed.errors = err.response.data.completed;
        checklistController.p.completed.model = false;
      })
      .finally(() => {
        setShowForceSubmitAlert(false);
      });
  };

  const RunInfo = ({ run_info }: { run_info: Run }) => {
    const skipYear =
      new Date(run_info.start_date).getFullYear() ===
      new Date(run_info.end_date).getFullYear();
    const start_date = dateString(run_info?.start_date, skipYear);
    const end_date = dateString(run_info?.end_date);
    const due_date = dateString(run_info?.due_date);
    const emptySpace = "\u00A0";
    return (
      <div dir="auto">
        <p className="mb-0">
          <b>{t("checklistDetail.team")} </b>
          {run_info.team_name}
        </p>
        <p>
          <small>
            {due_date === end_date
              ? t("checklistDetail.runDatesWithoutDueDate", {
                  start_date,
                  end_date,
                })
              : t("checklistDetail.runDatesWithDueDate", {
                  start_date,
                  end_date,
                  due_date,
                  emptySpace,
                })}
          </small>
        </p>
      </div>
    );
  };

  const ForceSubmitAlert = () => {
    const alertTitle = t("checklistDetail.form.forceSaveAlertTitle");
    const alertBody = t("checklistDetail.form.forceSaveAlertBody");
    const leftButton = (
      <Button
        onClick={() => setShowForceSubmitAlert(false)}
        variant="outline-dark"
        className="me-3 fs-8 px-3 py-1"
      >
        {t("checklistDetail.form.keepEditingBtn")}
      </Button>
    );
    const rightButton = (
      <Button
        onClick={() => submitChecklist(true)}
        variant="dark"
        className="fs-8 px-3 py-1"
      >
        {t("checklistDetail.form.forceSaveSubmitBtn")}
      </Button>
    );
    return (
      <Row>
        <Col>
          <CustomAlert
            className="mt-5"
            variant="warning"
            show={showForceSubmitAlert}
            alertTitle={alertTitle}
            alertBody={alertBody}
            showButtons={true}
            leftButton={leftButton}
            rightButton={rightButton}
          ></CustomAlert>
        </Col>
      </Row>
    );
  };

  return (
    <LoadSection controllers={[checklistController]}>
      {() => (
        <Container>
          <Row>
            <Col dir="auto">
              <h1 className="fw-bold">
                <Title text={checklistController.x!.name} />
              </h1>
              {checklistController.x?.run && (
                <RunInfo run_info={checklistController.x?.run} />
              )}
              <Body text={checklistController.x!.body} />
            </Col>
          </Row>
          <Row>
            <small className="text-muted mb-4">
              {t("checklistDetail.form.completeOnTotal", {
                completedTasks: checklistState.completedTasks,
                totalTasks: checklistState.totalTasks,
              })}
            </small>
          </Row>
          <Paginated controller={tasksController}>
            <Accordion
              className="px-0 mx-3 mb-3 border-bottom border-2 border-light-navy"
              defaultActiveKey={checklistState.tasksWithInteraction}
              alwaysOpen
              flush
            >
              {tasksController.list.map((tc) => (
                <>
                  {tc.p.response.errors?.map((error) => (
                    <Row key={error} className="mt-3">
                      <Col>
                        <CustomAlert className="pb-0 d-flex" variant="danger">
                          <p>{error}</p>
                        </CustomAlert>
                      </Col>
                    </Row>
                  ))}
                  <TaskWithSubtasks
                    key={tc.x!.id}
                    taskController={tc}
                    apiEndpoint={tasksApiEndpoint}
                    updateControllers={updateControllers}
                  />
                </>
              ))}
            </Accordion>
          </Paginated>
          {!checklistController.p.completed.errors?.length &&
            checklistController.p.completed.model && (
              <Row className="mt-3">
                <Col>
                  <CustomAlert className="pb-0 d-flex" variant="success">
                    <p>{t("checklistDetail.form.checklistCompleted")}</p>
                  </CustomAlert>
                </Col>
              </Row>
            )}
          {checklistController.p.completed.errors?.map((error) => (
            <Row key={error} className="mt-3">
              <Col>
                <CustomAlert className="pb-0 d-flex" variant="danger">
                  <p>{error}</p>
                </CustomAlert>
              </Col>
            </Row>
          ))}
          {!readOnly.x!.value && (
            <>
              <ForceSubmitAlert />
              {!showForceSubmitAlert &&
                (!checklistController.p.completed.model ||
                  checklistController.p.completed.errors?.length !== 0) && (
                  <div>
                    <Row className="mt-2 pt-2">
                      <p>
                        <Trans i18nKey="checklistDetail.form.autoSaveNotice" />
                      </p>
                    </Row>
                    <Row className="mt-3">
                      <Col md={6}>
                        <Button
                          variant="primary"
                          onClick={() => submitChecklist()}
                          disabled={checklistState.failed}
                        >
                          {submissionCount.p.num.model > 0
                            ? t("checklistDetail.form.submitBtnAgain")
                            : t("checklistDetail.form.submitBtn")}
                        </Button>
                      </Col>
                    </Row>
                    {pendingTasks > 0 && (
                      <Row className="mt-2 pt-2">
                        <p className="fs-8">
                          {t("checklistDetail.form.pendingTasks", {
                            pendingTasks,
                            itemsText: items_text,
                          })}
                        </p>
                      </Row>
                    )}
                  </div>
                )}
            </>
          )}
        </Container>
      )}
    </LoadSection>
  );
};
