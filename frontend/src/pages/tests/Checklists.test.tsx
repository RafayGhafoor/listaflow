import axios from 'axios'
import MockAdapter from "axios-mock-adapter";
import {Checklists, defaultCounts} from "../Checklists";
import {render} from '../../utils/test-utils';
import {act} from "@testing-library/react";

const mockSetParams = jest.fn()
const mockParams = {
  set: jest.fn(),
  get: jest.fn(),
  getAll: jest.fn(),
}
jest.mock('react-router-dom', () => ({
  ...(jest.requireActual('react-router-dom') as object),
  useSearchParams: () => [mockParams, mockSetParams]
}));

describe('Checklists.tsx', () => {
  const mockAdapter: MockAdapter = new MockAdapter(axios);
  beforeEach(() => {
    mockSetParams.mockReset()
    mockParams.set.mockClear()
    mockParams.get.mockClear()
    mockParams.getAll.mockClear()
    mockParams.get.mockImplementation(() => null)
    mockParams.getAll.mockImplementation(() => [])
    mockAdapter.reset()
    mockAdapter.onGet('/api/workflow/checklist/list/').reply(200, {results: []})
    mockAdapter.onGet('/api/workflow/checklist/list/count/').reply(200, defaultCounts())
  })
  it('Loads tabs when not in archive mode', async () => {
    const result = render(
      <Checklists isArchived={false}/>
    )
    expect(result.queryByText('checklists.tab.toDo')).toBeTruthy()
  })
  it('Does not load tabs when in archive mode', async () => {
    const result = render(
      <Checklists isArchived={true}/>
    )
    expect(result.queryByText('checklists.tab.toDo')).toBe(null)
  })
  it('Sets the default list as TO_DO when leaving archive mode.', async () => {
    const result = render(
      <Checklists isArchived={true}/>
    )
    expect(mockParams.set).not.toHaveBeenCalled()
    await act(() => result.getByText('userChecklist.archivedListTitle').click())
    await act(() => result.getByText('userChecklist.activeListTitle').click())
    expect(mockParams.set).toHaveBeenCalledWith('list_name', 'TO_DO')
    expect(mockParams.set).not.toHaveBeenCalledWith('list_name', 'ALL')
  })
  it('Sets the default list as ALL when entering archive mode.', async () => {
    const result = render(
      <Checklists isArchived={false}/>
    )
    expect(mockParams.set).not.toHaveBeenCalled()
    await act(() => result.getByText('userChecklist.activeListTitle').click())
    await act(() => result.getByText('userChecklist.archivedListTitle').click())
    expect(mockParams.set).toHaveBeenCalledWith('list_name', 'ALL')
    expect(mockParams.set).not.toHaveBeenCalledWith('list_name', 'TO_DO')
  })
})
