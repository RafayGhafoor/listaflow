import { fireEvent, render, screen } from "../../../utils/test-utils";
import { TagFilter } from "../TagFilter";

let mockReadyVariable: Boolean = true;
let selectedTags = ["tag1"];
const onChangeTestFunc = jest.fn();

// mock useList from providence
jest.mock("@opencraft/providence/react-plugin", () => {
  return {
    useList: () => {
      return {
        getOnce: () => {},
        get: () => {
          return { catch: () => {} };
        },
        errors: { messages: ["this is an error"] },
        ready: mockReadyVariable,
        list: [
          {
            x: {
              id: 1,
              name: "tag1",
            },
            p: { response: {} },
          },
          {
            x: {
              id: 2,
              name: "tag2",
            },
            p: { response: {} },
          },
        ],
      };
    },
  };
});

describe("TagFilter component", () => {
  beforeEach(() => {
    render(
      <TagFilter
        selectedTags={selectedTags}
        checklistDefinition={"dummy-cd"}
        onChange={onChangeTestFunc}
      />
    );
  });

  it("should render tags correctly", async () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag1 = screen.getByText("tag1");
    const tag2 = screen.getByText("tag2");
    expect(tag1).toBeInTheDocument();
    expect(tag2).toBeInTheDocument();
  });

  it("should render selected tags as checked", async () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag1Checkbox = screen.getByText("tag1").parentElement
      ?.previousElementSibling as HTMLElement;
    expect(tag1Checkbox).toHaveAttribute("checked");
    const tag2Checkbox = screen.getByText("tag2").parentElement
      ?.previousElementSibling as HTMLElement;
    expect(tag2Checkbox).not.toHaveAttribute("checked");
  });

  it("should trigger change function on changing tag selection", async () => {
    const toggleButton = screen.getByTestId("tag-filter-button");
    fireEvent.click(toggleButton);
    const tag2Checkbox = screen.getByText("tag2").parentElement
      ?.previousElementSibling as HTMLElement;
    fireEvent.click(tag2Checkbox);
    expect(onChangeTestFunc).toHaveBeenCalled();
  });
});
