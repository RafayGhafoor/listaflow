import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom"; // import MemoryRouter

import { Footer } from "../Footer";

jest.mock("../../constants/urls", () => ({
    TOS: "/terms-of-service",
    PRIVACY_POLICY: "",
  }));


describe("Footer component", () => {
    it("renders the terms of service link if the URL is provided", () => {
        render(<MemoryRouter><Footer /></MemoryRouter>);
        screen.debug();
        const tosLink = screen.getByRole("link", { name: /footer.tos/i });
        expect(tosLink).toHaveAttribute("href", "/terms-of-service"); // use the mock value
    });

    it("renders the privacy policy link if the URL is provided", () => {
        render(<MemoryRouter><Footer /></MemoryRouter>);
        const privacyLink = screen.queryByRole("link", { name: /footer.privacy/i });
        expect(privacyLink).not.toBeInTheDocument();
    });

    afterAll(() => {
        jest.clearAllMocks();
    });
});
