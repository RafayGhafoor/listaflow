import { useForm } from "@opencraft/providence/react-plugin";
import { useTranslation } from "react-i18next";
import { fireEvent, render, screen, waitFor } from "../../utils/test-utils";
import { Password } from "../Password";

interface TestFormState {
  password: string;
}

const { t } = useTranslation();

const TestComponent = ({ showMeter }: { showMeter: boolean }) => {
  const fakeController = useForm<TestFormState>("fakeController", {
    endpoint: "#",
    fields: {
      password: {
        value: "",
        validators: [
          { name: "required" },
          { name: "length", args: { min: 8 } },
        ],
      },
    },
  });

  return (
    <Password
      fielder={fakeController.f.password}
      label="test_password_label"
      showMeter={showMeter}
    />
  );
};

test("test password with no meter", async () => {
  render(<TestComponent showMeter={false} />);

  const field = screen.getByText(/test_password_label/i);
  expect(field).toBeInTheDocument();

  fireEvent.blur(screen.getByLabelText("test_password_label"));
  await waitFor(() => screen.getByText(/required/i));

  const error = screen.getByText(/required/i);
  expect(error).toBeInTheDocument();

  const error2 = screen.getByText(/must contain at least/i);
  expect(error2).toBeInTheDocument();
});

test("test password with meter", async () => {
  const { queryByText } = render(<TestComponent showMeter={true} />);

  const field = screen.getByText(/test_password_label/i);
  expect(field).toBeInTheDocument();

  fireEvent.change(screen.getByLabelText("test_password_label"), {
    target: { value: "123" },
  });
  await waitFor(() =>
    expect(
      screen.getByText(`${t("password.meter.tooShort")}`)
    ).toBeInTheDocument()
  );
  fireEvent.blur(screen.getByLabelText("test_password_label"));
  await waitFor(() => {
    expect(
      screen.getByText(/The password must contain at least 8 characters/i)
    ).toBeInTheDocument();
    expect(
      queryByText(/Enter a password with at least 8 characters/i)
    ).toBeNull();
  });

  fireEvent.change(screen.getByLabelText("test_password_label"), {
    target: { value: "123sadadf23zsdas" },
  });
  fireEvent.focus(screen.getByLabelText("test_password_label"));
  await waitFor(() => {
    expect(screen.getByText(t("password.meter.strong"))).toBeInTheDocument();
    expect(
      screen.getByText(t("password.validation.minLength"))
    ).toBeInTheDocument();
  });

  fireEvent.click(screen.getByText(t("password.showBtn")));
  await waitFor(() => {
    expect(
      screen.getByLabelText("test_password_label").getAttribute("type") ===
        "text"
    ).toBeTruthy();
  });

  fireEvent.click(screen.getByText(t("password.hideBtn")));
  await waitFor(() => {
    expect(
      screen.getByLabelText("test_password_label").getAttribute("type") ===
        "password"
    ).toBeTruthy();
  });
});
