import { TaskBase } from "./Task";
import { Assignee } from "./User";

export declare interface ReportFilter {
  id?: string;
  team_ids?: string[];
  start_date?: Date;
  end_date?: Date;
}

export declare interface ResponseRate {
  completed_count: number;
  total_count: number;
  timestamp?: string;
}

export declare interface NumberBucket {
  count: number;
  number: number;
}

export declare interface RatingBucket {
  count: number;
  rating: number;
}

export declare interface InterfaceStats {
  mean: number;
  median: number;
  mode: number;
  count: number;
  buckets: Array<NumberBucket> | Array<RatingBucket>;
}

export declare interface TaskTrend {
  task_definition: TaskBase;
  response_rate: ResponseRate;
  interface_stats: InterfaceStats;
}

export declare interface TrendsReport {
  id: string;
  response_rates: Array<ResponseRate>;
  trends: Array<TaskTrend>;
}

export declare interface UserTaskResponse {
  [date_header: string]: string;
  assignee_name: string;
}

export declare interface UserTaskResponseMap {
  [username: string]: UserTaskResponse;
}

export declare interface CompareReportTable {
  completed: number;
  total: number;
  dates: string[];
  tasks: UserTaskResponseMap;
  interface_type: string;
  task_label: string;
}

export declare interface CompareReport {
  [task_id: string]: CompareReportTable;
}

export declare interface OverviewReport {
  id: string;
  start_date: Date;
  end_date: Date;
  team_name: string;
  completed_checklist_count: number;
  total_checklist_count: number;
  assignees: Array<Assignee>;
}
