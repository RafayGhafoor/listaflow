import {ReactComponentElement, ReactNode} from "react";
import { render, RenderOptions } from "@testing-library/react";
import {
  defaultContextValues,
  ProvidenceContext,
} from "@opencraft/providence/react-plugin/context";
import { Provider } from "react-redux";
import { createStore } from "redux-dynamic-modules";
import { fieldLength, requiredField } from "./validators";
import { BrowserRouter } from "react-router-dom";
import { ContextRenderOptions } from "@opencraft/providence/react-plugin/specs/helpers"

export const customRender = (ui: ReactComponentElement<any>, options: Omit<RenderOptions, "wrapper"> & ContextRenderOptions = {}) => {
  const defaultContext = defaultContextValues()
  defaultContext.validators.required = requiredField
  defaultContext.validators.length = fieldLength
  const {context, store} = options
  const builtContext = {...defaultContext, context}
  const targetStore = store || createStore({})
  const wrapper = ({children}: {children: ReactNode}) => (
    <Provider store={targetStore}>
      <ProvidenceContext.Provider value={builtContext}>
        <BrowserRouter>{children}</BrowserRouter>
      </ProvidenceContext.Provider>
    </Provider>
  );
  return render(ui, {wrapper})
}

export * from "@testing-library/react";
export { customRender as render };

const scheduler =
  typeof setImmediate === "function" ? setImmediate : setTimeout;

export const flushPromises = () => {
  return new Promise(function (resolve) {
    scheduler(resolve);
  });
};
