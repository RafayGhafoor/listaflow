# Listaflow Frontend

Prerequisites

The following must be installed:

- Node.js version: v16.xx.x

Install frontend dependencies using below command:

```
make install_frontend_dependencies
```

To start a development server run:

```
make run.frontend
```

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

You can test frontend with:

```
make test.frontend
```

You can also build frontend assets with:

```
make build.frontend
```
