# SSO Support

Listaflow supports Google SSO for now.

**Google SSO**

To enable google authentication

- Create OAuth client ID credentials by following this
  [guide](https://developers.google.com/workspace/guides/create-credentials#oauth-client-id)
- Copy client ID
- Paste it in `frontend/.env` or `frontend/.env.local` file as shown below.

```sh
REACT_APP_GOOGLE_CLIENT_ID=xxxxxxxxxxx-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.apps.googleusercontent.com
```
